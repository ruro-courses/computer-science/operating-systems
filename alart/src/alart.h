#ifndef _ALART_H
#define _ALART_H
#include <time.h>

typedef struct
{
    char type;
    unsigned threshold;
    time_t timeout;
    unsigned offset;
    time_t (*timestamps)[];
} EventState;

int throw(EventState *, char);
void alart(void);

#endif
