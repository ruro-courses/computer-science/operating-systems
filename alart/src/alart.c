#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "terminator.h"
#include "alart.h"

void alart(void)
{
    static int isRandom = 0;
    if (!isRandom)
    {
        isRandom = 1;
        srand(time(NULL));
    }
    int opt = rand() % 3;
    const char m_alart[] =
#include "ascii/alart"
        ;
    const char m_dangar[] =
#include "ascii/dangar"
        ;
    const char m_halt[] =
#include "ascii/halt"
        ;
    switch (opt)
    {
        case 0:
            printf("\n%s", m_alart);
            break;
        case 1:
            printf("\n%s", m_dangar);
            break;
        case 2:
        default:
            printf("\n%s", m_halt);
            break;
    }
    fflush(stdout);
    asm volatile("hlt");
}

int throw(EventState *event, char type)
{
    // Search for event in EventState.
    EventState *e = event;
    while (e->type && e->type != type)
        e++;
    if (!e->type)
        terminate(0, "Event with id `%u` is not defined.\n", type);

    // Allocate memory on first event of this type.
    if (!e->timestamps)
        e->timestamps = calloc(e->threshold, sizeof(**e->timestamps));

    // Update the current event.
    time_t now = time(NULL);
    time_t *ts = *e->timestamps;
    e->offset += 1;
    e->offset %= e->threshold;
    ts[e->offset] = now;

    // Check if an alart should happen.
    e = event;
    while (e->type)
    {
        // Event didn't happen even once.
        if (!e->timestamps)
            return 0;

        // Oldest event is older than the timeout.
        ts = *e->timestamps;
        unsigned oldest = (e->offset + 1) % e->threshold;
        if (ts[e->offset] - ts[oldest] > e->timeout)
            return 0;

        e++;
    }

    // All events are newer than timeout.
    // !!! ALART !!!
    alart();
    return 1;
}
