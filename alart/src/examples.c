#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <termios.h>

#include "alart.h"

int main(void)
{
    // Setup terminal for demonstration, not related to alart.h //
    static struct termios oldt, newt;
    tcgetattr( STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON);
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);
    // Setup terminal for demonstration, not related to alart.h //

    // The throw method accepts a null-terminated array of EventStates.
    EventState st[] =
    {
        {
            .type = 'a',        // Type of event. (A char)
            .threshold = 2,     // Number of events of needed for the alart.
            .timeout = 10,      // Time in seconds, which invalidates an event.
            .offset = 0,        // Starting offset, should be 0.
            .timestamps = NULL  // Pointer to state timestamps, should be NULL.
        },
        {
            .type = 'p',
            .threshold = 3,     // Different thresholds,
            .timeout = 10,      // and timeouts are possible.
            .offset = 0,
            .timestamps = NULL
        },
        {0} // Array is null-terminated!
    };

    char temp;
    while ((temp = getchar()))
    {
        if (!isspace(temp) && temp != EOF)
            throw(st, temp);
    }
}
