#ifndef _BINGRAPH_H // INCLUDE GUARD
#define _BINGRAPH_H

// Number of iterations performed for approximation.
#define STD_BIN_ITER 1 << 20

// Height of graph in chars.
#define STD_GRPH_H 40

// Height multiplier.
#define STD_GRPH_MULT 4

// See the theme files for instructions, on how to use themes.
#ifndef _THEME
#error Please import a theme file!
#endif

// Theme exports.
const char **points;
const char *above;
const char *below;
const unsigned maxchar;
const unsigned pointopts;
const char *frame_ul;
const char *frame_uc;
const char *frame_ur;
const char *frame_cr;
const char *frame_br;
const char *frame_bc;
const char *frame_bl;
const char *frame_cl;

// Binomial graph structure.
typedef struct
{
    unsigned trials_num;
    unsigned iterations_num;
    double success_prob;
    double results[];
} BinGraph;

void testBinGraph(BinGraph *);
char *drawGraph(BinGraph *);
char *getGraphString(unsigned, double);

#endif // INCLUDE GUARD 
