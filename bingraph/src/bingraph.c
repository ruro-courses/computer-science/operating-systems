#include <stdio.h>
#include <string.h>

#define _THEME -1
#include "bingraph.h"
#include "randm.h"

char *getGraphString(unsigned trials, double success)
{
    BinGraph *bin = NULL;
    if (!trials || success > 1.0 || success < 0.0)
        return NULL;

    size_t size = sizeof(*bin) + sizeof(bin->results[0])*(trials+1);
    bin = malloc(size);
    if (!bin)
        return NULL; 

    memset(bin, 0, size);
    bin->success_prob = success;
    bin->trials_num = trials;
    bin->iterations_num = STD_BIN_ITER;

    testBinGraph(bin);
    char *str = drawGraph(bin);
    free(bin);

    return str;
}

void testBinGraph(BinGraph *bin)
{
    unsigned trials = bin->trials_num;
    unsigned iterations = bin->iterations_num;
    double success = bin->success_prob;
    double step = (double)STD_GRPH_MULT/iterations;
    unsigned accum = 0;

    for (unsigned i = 0; i < iterations; i++)
    {
        for (unsigned j = 0; j < trials; j++)
             accum += (randd_f() < success);

        bin->results[accum] += step;
        accum = 0;
    }
}

const char *getPointSymbol(double val)
{
    val *= pointopts;
    ssize_t idx = (int) val;
    if (idx >= 0 && idx < pointopts)
        return points[idx];
    else
        return points[0];
}

#define append(symbol)                                          \
    do {                                                        \
        snprintf(str + i, size - i, "%s%n", symbol, &tmp);      \
        i += tmp;                                               \
    } while(0)

char *drawGraph(BinGraph *bin)
{
    size_t i = 0;
    int tmp = 0;
    char *str = NULL;
    const double step = (double)1/STD_GRPH_H;
    unsigned trials = bin->trials_num;
    size_t size = (maxchar) * (STD_GRPH_H + 2) * (trials + 4) + 1;

    // STD_GRPH_H rows of data.
    // trials     columns of data.
    // +2         rows of formatting.
    // +2         columns of formatting.  (+1 '\n')
    // Each symbol is maxchar bytes wide. (+1 '\0')
    str = malloc(size);
    if (!str)
        return NULL;


    // Print upper part of graph frame. 
    append(frame_ul);
    for (unsigned col = 0; col <= trials; col++)
        append(frame_uc);
    append(frame_ur);
    append("\n");

    // Print the middle rows.
    for (unsigned row = 0; row < STD_GRPH_H; row++)
    {
        append(frame_cl);
        // Print the data for this row.
        double high = 1 - row*step;
        double low  = 1 - (row + 1)*step;
        for (unsigned col = 0; col <= trials; col++)
        {
            double res = bin->results[col];
            if (res > high)
                append(below); // symbol is *below* the point
            else if (res < low)
                append(above); // symbol is *above* the point
            else 
                append(getPointSymbol((res-low)/step));
        }
        append(frame_cr);
        append("\n");
    }

    // Print bottom part of graph frame. 
    append(frame_bl);
    for (unsigned col = 0; col <= trials; col++)
        append(frame_bc);
    append(frame_br);
    append("\n");

    return str;
}
