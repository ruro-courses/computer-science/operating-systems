#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Try with any one of these lines uncommented
#include "themes/wide.h"
//#include "themes/plot.h"
//#include "themes/cadre.h"
//#include "themes/ascii_plot.h"
//#include "themes/ascii_cadre.h"

#include "bingraph.h"

int main(void)
{
    srand(time(NULL));
    for (int i = 1; i < 4; i++)
    {
        char *str = getGraphString(75, 0.25*i);
        printf("%s\n", str);
        free(str);
    }
}
