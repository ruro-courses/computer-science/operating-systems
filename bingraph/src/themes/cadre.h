#ifdef _THEME
#error Only 1 theme allowed.
#else
#define _THEME

// Array of strings, representing the points on the graph.
// The symbols used for displaying a point on a graph.
// If more than one symbol is defined, it is interpreted as different symbols,
// for points ranging from "less than a full symbol" to "full symbol".
const char *pts[] = // ['●']
{
    "\xe2\x97\x8f"
};

// above is the string for symbols placed *above* the actual point.
// below is the string for symbols placed *below* the actual point.
const char *above = " "; // ' '
const char *below = " "; // ' '

// The maximum number of chars one "symbol" uses in this theme.
const unsigned maxchar = 3;

// Array of strings, representing the frame of the graph.
const char *frame_ul = "\xe2\x95\xad";  // Upper-Left       '╭'
const char *frame_uc = "\xe2\x94\x80";  // Upper-Center     '─'
const char *frame_ur = "\xe2\x95\xae";  // Upper-Right      '╮'
const char *frame_cr = "\xe2\x94\x82";  // Center-Right     '│'
const char *frame_br = "\xe2\x95\xaf";  // Bottom-Right     '╯'
const char *frame_bc = "\xe2\x94\x80";  // Bottom-Center    '─'
const char *frame_bl = "\xe2\x95\xb0";  // Bottom-Left      '╰'
const char *frame_cl = "\xe2\x94\x82";  // Center-Left      '│'

// Keep the next 2 lines unchanged, exports for the points array.
const unsigned pointopts = sizeof(pts)/sizeof(*pts);
const char **points = pts;

#endif
