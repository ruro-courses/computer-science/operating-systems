#ifdef _THEME
#error Only 1 theme allowed.
#else
#define _THEME

// Array of strings, representing the points on the graph.
// The symbols used for displaying a point on a graph.
// If more than one symbol is defined, it is interpreted as different symbols,
// for points ranging from "less than a full symbol" to "full symbol".
const char *pts[] = // ['.','_','-','=']
{
    ".",
    "_",
    "-",
    "="
};

const char **points = pts;

// above is the string for symbols placed *above* the actual point.
// below is the string for symbols placed *below* the actual point.
const char *above = " "; // ' '
const char *below = "*"; // '*'

// The maximum number of chars one "symbol" uses in this theme.
const unsigned maxchar = 1;

// Keep this line unchanged, exports the number of available point options.
const unsigned pointopts = sizeof(pts)/sizeof(*pts);

// Array of strings, representing the frame of the graph.
const char *frame_ul = "^"; // Upper-Left       '^'
const char *frame_uc = " "; // Upper-Center     ' '
const char *frame_ur = " "; // Upper-Right      ' '
const char *frame_cr = " "; // Center-Right     ' '
const char *frame_br = ">"; // Bottom-Right     '>'
const char *frame_bc = "-"; // Bottom-Center    '-'
const char *frame_bl = "+"; // Bottom-Left      '+'
const char *frame_cl = "|"; // Center-Left      '|'

#endif
