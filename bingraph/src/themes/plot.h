#ifdef _THEME
#error Only 1 theme allowed.
#else
#define _THEME

// Array of strings, representing the points on the graph.
// The symbols used for displaying a point on a graph.
// If more than one symbol is defined, it is interpreted as different symbols,
// for points ranging from "less than a full symbol" to "full symbol".
const char *pts[] = // ['▁', '▂', '▃', '▄', '▅', '▆', '▇', '█']
{
    "\xe2\x96\x81",
    "\xe2\x96\x82",
    "\xe2\x96\x83",
    "\xe2\x96\x84",
    "\xe2\x96\x85",
    "\xe2\x96\x86",
    "\xe2\x96\x87",
    "\xe2\x96\x88"
};

const char **points = pts;

// above is the string for symbols placed *above* the actual point.
// below is the string for symbols placed *below* the actual point.
const char *above = " ";            // ' '
const char *below = "\xe2\x96\x88"; // '█'

// The maximum number of chars one "symbol" uses in this theme.
const unsigned maxchar = 3;

// Keep this line unchanged, exports the number of available point options.
const unsigned pointopts = sizeof(pts)/sizeof(*pts);

// Array of strings, representing the frame of the graph.
const char *frame_ul = "\xe2\x96\xb2";  // Upper-Left       '▲'
const char *frame_uc = " ";             // Upper-Center     ' '
const char *frame_ur = " ";             // Upper-Right      ' '
const char *frame_cr = " ";             // Center-Right     ' '
const char *frame_br = "\xe2\x96\xb6";  // Bottom-Right     '▶'
const char *frame_bc = "\xe2\x95\x90";  // Bottom-Center    '═'
const char *frame_bl = "\xe2\x95\xac";  // Bottom-Left      '╬'
const char *frame_cl = "\xe2\x95\x91";  // Center-Left      '║'

#endif
