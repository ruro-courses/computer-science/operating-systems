#ifndef _FORKRING_H
#define _FORKRING_H

typedef struct {
    int r;
    int w;
} Pipe;

typedef void func(int, int, void *);

void forkIO(Pipe, Pipe, func, void *) __attribute__(( nonnull (3) ));
Pipe mkPipe(void);

#endif
