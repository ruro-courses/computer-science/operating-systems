#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include "forkring.h"
#include "terminator.h"

Pipe mkPipe(void)
{
    int fd[2];
    if (pipe(fd))
        terminate(errno, "Couldn't create a pipe.\n");
    Pipe ret = { .r = fd[0], .w = fd[1] };
    return ret;
}

void forkIO(Pipe in, Pipe out, func run, void *data)
{
    pid_t p = fork();
    if (p == -1)
        terminate(errno, "Couldn't fork.\n");
    if (!p)
    {
        // Child.
        if (close(in.w))
            terminate(errno, "Couldn't close pipe end.\n");
        if (close(out.r))
            terminate(errno, "Couldn't close pipe end.\n");
        run(in.r, out.w, data);
        exit(0);
    }
}
