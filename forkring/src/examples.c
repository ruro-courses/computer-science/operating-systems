#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <inttypes.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

#include "forkring.h"
#include "terminator.h"

#define sread(r, s)  ( read(r, &s, sizeof(s)) != sizeof(s))
#define swrite(w, s) (write(w, &s, sizeof(s)) != sizeof(s))

func pass;
void pass(int i, int o, void *data)
{
    // Get pid and in-order number.
    intmax_t self = (intmax_t)getpid();
    size_t id = *(size_t *)data;
    announce("[%6jd|%4zu] Starting process.\n", self, id);
    while (1)
    {
        // Get counter from previous process.
        ssize_t counter;
        if (sread(i, counter))
            terminate(errno, "Couldn't read from pipe.\n");
        if (counter)
            announce("[%6jd|%4zu] Counter is %zd\n", self, id, counter);
        counter = counter ? counter - 1 : 0;

        // Write to next process.
        if (swrite(o, counter))
            terminate(errno, "Couldn't write to pipe.\n");

        // Stop, when 0 is reached.
        if (!counter)
            break;
    }
    announce("[%6jd|%4zu] Stopping...\n", self, id);
}

int main(void)
{
    // Make stdout and stderr atomic.
    atomize();

    // Get user input for process number.
    size_t K;
    announce("Please, input the number of processes in the ring:\n");
    if (scanf("%zu", &K) != 1)
        terminate(errno, "Couldn't get user input.\n");
    if (!K)
        terminate(0, "K should be greater than 0.\n");

    // Create first/last pipe.
    Pipe prev, next, first;
    first = mkPipe();
    prev.r = dup(first.r);
    prev.w = dup(first.w);

    // For all the processes except the last one.
    size_t i;
    for (i = 1; i < K; i++)
    {
        // Spawn process with input from prev and output to next.
        next = mkPipe();
        forkIO(prev, next, pass, &i);
        prev = next;
    }

    // Last process writes to first process.
    forkIO(prev, first, pass, &i);

    // Get user input for counter.
    size_t N;
    sleep(1);
    announce("Please, input the starting counter value:\n");
    if (scanf("%zu", &N) != 1)
        terminate(errno, "Couldn't get user input.\n");

    // Kick start cycle.
    if (swrite(first.w, N))
        terminate(errno, "Couldn't write to first process.\n");

    // Wait for all children to safely stop.
    while (wait(NULL) != -1 && errno != ECHILD);
}
