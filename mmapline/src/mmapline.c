// Enable LFS for 32-bit systems.
#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <inttypes.h>

size_t TAIL_SIZE = 5;

void terminate(int code, char *fmt, ...)
{
    // Print the formatted message.
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    
    // Print the error message, if any.
    if (code) {
        printf("Error: %s \n", strerror(code));
    }

    exit(code);
}

void print_line_info(char *lineend, off_t linenum, size_t linelen)
{
    // Print right justified line info.
    printf("%15jd |%15jd | '", (intmax_t)linenum, (intmax_t)linelen);
    
    // Print TAIL_SIZE or less characters from end of line.
    off_t tail = (linelen < TAIL_SIZE) ? linelen : TAIL_SIZE;
    while (tail) {
        // Prints real characters for printable ASCII, hex values otherwise.
        char c = *(lineend - tail);
        if (isprint(c)) { putchar(c); }
        else { printf("\\x%02x", c & 0xFF); }
        tail--;
    }
    printf("'\n");
}

void process_line(char *file, size_t filesize)
{
    ssize_t linelen = 0;
    size_t  linenum = 1;
    // Filesize is 1 greater, to take into account EOF.
    filesize++;
    
    // Print the table header.
    printf("%15s |%15s |%16s\n", "Line #", "Line Length", "Line End");

    while (filesize) {
        if (*file == '\n' || filesize == 1) {
            print_line_info(file, linenum, linelen);
            linelen = -1;
            linenum++;
        }
        linelen++;
        file++;
        filesize--;
    }
}

int main(int argc, char *argv[])
{
    int fd;
    char *file;
    struct stat fileprop;
    size_t filesize;

    // Check number of arguments.
    if (argc != 2 && argc != 3) {
        terminate(0, "Usage: %s FILE [LEN]\n", argv[0]);
    }
    
    if (argc == 3) {
        char *end;
        TAIL_SIZE = strtol(argv[2], &end, 10);
        if (strlen(argv[2]) != (end - argv[2])) {
            terminate(0, "Usage: %s FILE [LEN]\n", argv[0]);
        }
    }

    // Open file.
    fd = open(argv[1], O_RDONLY | O_LARGEFILE);
    if (fd == -1) {
        terminate(errno, "Can't open file '%s'.\n", argv[1]);
    }
    if (fstat(fd, &fileprop) == -1) {
        terminate(errno, "Can't get properties of file '%s'.\n", argv[1]);
    }
    filesize = (size_t)fileprop.st_size;

    // Mmap file.
    file = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE | MAP_NORESERVE, fd, 0);
    if (file == MAP_FAILED) {
        terminate(errno, "Unable to mmap file into memory.\n");
    }

    process_line(file, filesize);

    // Clean up, we don't care about errors on closing.
    munmap(file, filesize);
    close(fd);
    return 0;
}
