#ifndef _TERMINATOR_H
#define _TERMINATOR_H

void terminate(int code, const char *fmt, ...)
    __attribute__ ((noreturn))
    __attribute__ ((format (printf, 2, 3)));

#endif
