#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>
#include <string.h>

#include "finder.h"
#include "terminator.h"

void filels(const char *dirname, const char *filename, FileLsOptions opt)
{
    // Ignore . and ..
    if (!strncmp(".", filename, 3) || !strncmp("..", filename, 3))
        return;

    // Get the lenght of {dirname}/{filename}.
    size_t pathlen = 1 + snprintf(NULL, 0, "%s/%s", dirname, filename);
    pathlen = (pathlen > PATH_MAX) ? PATH_MAX : pathlen;

    // Obtain a string with full filename.
    char filepath[pathlen];
    snprintf(filepath, pathlen, "%s/%s", dirname, filename);

    // Get stat of flie.
    struct stat filestat;
    if (lstat(filepath, &filestat))
        terminate(errno, "Can't stat file '%s'.\n", filepath);

    // If file is too old, skip. (enabled with EXCL_OLD)
    if ((opt & EXCL_OLD) && filestat.st_mtime < (time(NULL) - OLD_TIME))
        return;

    // If user doesn't own the file, skip. (enabled with EXCL_OWN)
    if ((opt & EXCL_OWN) && filestat.st_uid != geteuid())
        return;

    // If file is hidden, skip. (enabled with EXCL_DOT)
    if ((opt & EXCL_DOT) && filename[0] == '.')
        return;

    // If regular file, skip. (enabled with EXCL_REG)
    if ((opt & EXCL_REG) && S_ISREG(filestat.st_mode))
        return;

    // If symlink, skip. (enabled with EXCL_SYM)
    if ((opt & EXCL_SYM) && S_ISLNK(filestat.st_mode))
        return;

    // If directory, skip. (enabled with EXCL_DIR)
    if ((opt & EXCL_DIR) && S_ISDIR(filestat.st_mode))
        return;

    // If its an `other` kind of file, skip. (enabled with EXCL_OTH)
    if ((opt & EXCL_OTH)
            && !S_ISREG(filestat.st_mode)
            && !S_ISLNK(filestat.st_mode)
            && !S_ISDIR(filestat.st_mode))
        return;

    // Print indentation.
    unsigned indent = opt&_IND_MSK;
    indent = (indent > MAX_INDL) ? MAX_INDL : indent;
    while (indent--)
        putchar(IND_CHAR);

    // Print filename.
    printf("%s\n", filename);

    // If directory, print all the subdirectories, but indented one more step.
    if (S_ISDIR(filestat.st_mode)) {
        // If we haven't filled the indentation bytes, increase indentation.
        if ((opt & _IND_MSK) != _IND_MSK)
            opt += _IND_ONE;
        finder(filepath, opt);
    }
}

void finder(const char *dirname, FileLsOptions opt)
{
    // Open directory.
    DIR *dir = opendir(dirname);
    if (!dir || !dirname)
        terminate(errno, "Can't open directory '%s'.\n", dirname);

    // For every file in the directory, list that file.
    struct dirent *file;
    while ((file = readdir(dir)))
        filels(dirname, file->d_name, opt);

    // Close directory.
    closedir(dir);
}
