#include "finder.h"
#include "terminator.h"

int main(int argc, char *argv[])
{
    if (argc != 2)
        terminate(0, "Usage: %s PATH\n", argv[0]);

    finder(argv[1], EXCL_SYM | EXCL_OLD | EXCL_OWN);
}
