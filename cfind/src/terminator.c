#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "terminator.h"

void terminate(int code, const char *fmt, ...)
{
    // Print the formatted message.
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);

    // Print the error message, if any.
    if (code)
        printf("Error: %s \n", strerror(code));

    exit(code);
}
