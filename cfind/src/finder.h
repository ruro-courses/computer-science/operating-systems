#ifndef _FINDER_H
#define _FINDER_H
typedef enum
{
    ALL      = 0x0000, // All files.
    EXCL_OLD = 0x0100, // Exclude files with changes older than OLD_TIME.
    EXCL_OWN = 0x0200, // Exclude files, not owned by current user.
    EXCL_DOT = 0x0400, // Exclude hidden files. (starting with a dot)
    EXCL_REG = 0x1000, // Exclude regular files from result.
    EXCL_SYM = 0x2000, // Exclude symlinks from result.
    EXCL_DIR = 0x4000, // Exclude directories from result. (doesn't recurse)
    EXCL_OTH = 0x8000, // Exclude block and character dev, FIFO, pipes, sockets.
    _IND_MSK = 0x00ff, // Bitmask for indentation bytes. (implementation only)
    _IND_ONE = 0x0001, // One level of indentation.      (implementation only)
} FileLsOptions;

// Defines section for settings, changeable at compile time.
#ifndef IND_CHAR // Character to use for indentation (def is space)
#define IND_CHAR ' '
#endif

#ifndef MAX_INDL // Max indentation level (def is 70 spaces for 80 char tty)
#define MAX_INDL 70
#endif

#ifndef OLD_TIME // Time in seconds, selects 'old' files (def is 2 weeks)
#define OLD_TIME 1209600
#endif

// Function declarations.
void finder(const char *dirname, FileLsOptions opt);
void filels(const char *dirname, const char *filename, FileLsOptions opt);
#endif
