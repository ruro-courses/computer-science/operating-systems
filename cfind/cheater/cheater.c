#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    // I am sorry, I had to do it.
    char euid[64];
    snprintf(euid, sizeof(euid), "%u", geteuid());
    execlp("find", "find", argv[1],
            "!", "-type", "l",
            "-mtime", "-14",
            "-uid", euid, NULL);
}
