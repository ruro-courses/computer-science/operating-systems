#include <errno.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "lnkdlst.h"

void destruct(lnkdlst lst)
{
    lnkdlst run;
    while (lst)
    {
        run = lst->next;
        free(lst);
        lst = run;
    }
}

int construct(lnkdlst *at_lst, ...)
{
    if (!at_lst || *at_lst)
    {
        errno = EINVAL;
        return -1; // E: at_lst should be a pointer to an empty lnkdlst.
    }

    // Allocate head of the list.
    lnkdlst run = *at_lst = malloc(sizeof(struct st_lnkdlst));
    if (!run)
        return -1; // E: malloc failed.

    va_list elements;
    va_start(elements, at_lst);
    char *elem;
    while ((elem = va_arg(elements, char *)))
    {
        run = run->next = malloc(sizeof(struct st_lnkdlst));
        if (!run)
        {
            va_end(elements);
            return -1; // E: malloc failed.
        }
        memcpy(&run->data, elem, sizeof(run->data));
    }
    va_end(elements);
    return 0;
}

int store(lnkdlst lst, const char *filename)
{
    // Open the file.
    FILE *fp;
    if (!(fp = fopen(filename, "w")))
        return -1; // E: fopen failed.

    // The first entry in the file is the size of data blocks.
    size_t data_size = sizeof(lst->data);
    fwrite(&data_size, sizeof(size_t), 1, fp);

    // Write the data contents to file sequentially.
    while (lst)
    {
        if (!fwrite(lst->data, data_size, 1, fp))
        {
            if (errno == EINTR)
                continue;
            return -1; // E: fwrite failed.
        }
        lst = lst->next;
    }

    fflush(fp);
    fclose(fp);
    return 0;
}

int load(lnkdlst *at_lst, const char *filename)
{
    if (!at_lst || *at_lst)
    {
        errno = EINVAL;
        return -1; // E: at_lst should be a pointer to an empty lnkdlst.
    }

    // Allocate head of the list.
    lnkdlst run = *at_lst = malloc(sizeof(struct st_lnkdlst));
    if (!run)
        return -1; // E: malloc failed.

    // Open the file.
    FILE *fp;
    if (!(fp = fopen(filename, "r")))
        return -1; // E: fopen failed.

    // The first entry in the file is the size of data blocks.
    size_t data_size;
    while (!fread(&data_size, sizeof(size_t), 1, fp))
    {
        if (errno == EINTR)
            continue;
        return -1; // E: fread failed.
    }

    // If it doesn't match the current size, something went wrong.
    if (data_size != sizeof(run->data))
    {
        errno = ERANGE;
        return -1; // E: DATA_SIZE is wrong.
    }

    // Read lnkdlst nodes from file.
    while (1)
    {
        while (!fread(&run->data, data_size, 1, fp))
        {
            if (errno == EINTR)
                continue;
            return -1; // E: fread failed.
        }

        if (feof(fp))
            break;

        run = run->next = malloc(sizeof(struct st_lnkdlst));
        if (!run)
            return -1; // E: malloc failed.
    }

    fclose(fp);
    return 0;
}
