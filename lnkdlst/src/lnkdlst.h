#ifndef _LNKDLST_H
#define _LNKDLST_H

// Define section, for compile time changeable options.
#ifndef DATA_SIZE // The size of the buffer, each lnkdlst node has.
#define DATA_SIZE 64
#endif

// Node of lnkdlst.
typedef struct st_lnkdlst
{
    struct st_lnkdlst *next;
    char data[DATA_SIZE];
} *lnkdlst;

// Function declarations.
// All functions return 0 on success and -1 otherwise.
// If -1 was returned, errno may be set to indicate what went wrong.

// Remove lnkdlst and its contents from memory.
void destruct(lnkdlst lst);

// Constructs a lnkdlst from a NULL-terminated list of elements.
// construct(lst, elem1, elem2, ..., NULL);
// (elements are pointers to char arrays of size DATA_SIZE)
int construct(lnkdlst *at_lst, ...)
    __attribute__((sentinel));

// Store the linked list and its contents to file filename.
int store(lnkdlst lst, const char *filename);

// Restore contents of lnkdlst from file filename.
int load(lnkdlst *at_lst, const char *filename);
#endif
