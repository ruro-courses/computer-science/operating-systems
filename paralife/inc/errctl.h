#ifndef _ERRCTL_H
#define _ERRCTL_H

#include <stdbool.h>

/// @addtogroup errctl
/// @{

/// @brief Report an error to the user and stop execution, if needed.
///
/// 1. Print a custom formatted message, specified by
///     format string `fmt` and extra '`...`' args.
/// 2. Print a standard error message for the error with code `errno`.
/// 3. @e If the error happened in @b `master`
///     - Terminate all the processes.
///     - Wait for them to finish.
/// 4. @e If the error happened in the child
///     - Print a message, asking the user to terminate manually.
///
/// @param master Wether the program, the controlling one.
/// @param code The errno value, that caused the critical error.
/// @param fmt Format string, compatible with the printf family.
/// @param ... Extra arguments, for the format string.
void error(bool master, int code, const char *fmt, ...)
    __attribute__ ((nonnull))
    __attribute__ ((noreturn))
    __attribute__ ((format (printf, 3, 4)));

/// @}
#endif
