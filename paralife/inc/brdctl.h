#ifndef _BRDCTL_H
#define _BRDCTL_H

#include "segctl.h"
#include "pacemaker.h"

#include <stdlib.h>

/// @addtogroup brdctl
/// @{

/// The structure, representing a whole Game of Life board.
typedef struct
{
    /// The name of the board.
    char *brdName;
    /// The associated @ref PaceMaker.
    PaceMaker *masterPace;
    /// An array of @ref Segment this board has.
    Segment **segArr;
    /// The size of each @ref Segment (in cells).
    ssize_t segSize;
    /// The size the board (in @ref Segment).
    ssize_t segNum;
} Board;

/// Load the board with name `boardName`
Board *createBoard(const char *boardName)
    __attribute__ ((nonnull));

/// Remove the board (from memory)
void deleteBoard(Board *brd)
    __attribute__ ((nonnull));

/// Start the PaceMaker and Segment processes associated with the board
void runBoard(Board *brd, PaceAction *actions)
    __attribute__ ((nonnull));

/// Storage for the currently loaded board.
extern Board *curBoard;

/// @}
#endif
