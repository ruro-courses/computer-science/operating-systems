#ifndef _LIFERULES_H
#define _LIFERULES_H

#include <stdbool.h>
#include <stdlib.h>

/// @addtogroup liferules
/// @{

/// @brief Possible values for the Game of Life board cells.
///
/// The rules of Game of Life are as follows:
///     1. A @ref CE_DEAD cell changes into a @ref CE_LIVE cell if
///         \em exactly 3 of its neighbours are @ref CE_LIVE.
///         It comes alive, as if by reproduction.
///     2. A @ref CE_LIVE cell changes into a @ref CE_DEAD cell if
///         &lt;2 or &gt;3 of its neighbours are @ref CE_LIVE.
///         It dies, as if by starving or out of loneliness.
///     3. The very first turn a @ref CE_LIVE cell is @ref CE_BORN.
///         This is a purely cosmetic feature, @ref CE_BORN cells act exactly as
///         @ref CE_LIVE cells.
///     4. The very first turn a @ref CE_DEAD cell is @ref CE_FADE.
///         This is a purely cosmetic feature, @ref CE_FADE cells act exactly as
///         @ref CE_DEAD cells.
///     5. The @ref CE_WALL cells don't contribute towards their neighbours life
///         and act as permanently dead cells.
///
/// As such, here is a diagram of all possible state changes:
/// @dot
/// digraph {
///     layout=circo
///     W [shape=circle, fixedsize=true, URL="@ref CE_WALL"]
///     L [shape=circle, fixedsize=true, URL="@ref CE_LIVE"]
///     D [shape=circle, fixedsize=true, URL="@ref CE_DEAD"]
///     B [shape=circle, fixedsize=true, URL="@ref CE_BORN"]
///     F [shape=circle, fixedsize=true, URL="@ref CE_FADE"]
///     F -> {D, B}
///     B -> {L, F}
///     L -> F
///     D -> B
///     W -> W
/// }
/// @enddot
/// @note The `__packed__` attribute tries to enforce `char`-sized enums.
typedef enum __attribute__ ((__packed__))
{
    /// A dead cell.
    CE_DEAD,
    /// A dead cell, that \em just died.
    CE_FADE,
    /// A living cell.
    CE_LIVE,
    /// A living cell, that was \em just born.
    CE_BORN,
    /// An edge of the board, or separator.\n
    /// Acts as a @ref CE_DEAD cell for others, but can't become @ref CE_LIVE.
    CE_WALL
} Cell;

/// @brief Computes the next state of a cell, based on its current state and
///     its surroundings.
///
/// @param self The current state of the cell.
/// @param surr The number of surrounding cells, which are alive.
/// @return The state of the cell for the next step. 
Cell getNextStep(Cell self, ssize_t surr);

/// @brief Determines, if the cell can be considered "alive".
///
/// @ref CE_LIVE and @ref CE_BORN are considered "alive", other cells are dead. 
///
/// @param inspect The cell to inspect.
/// @return Whether it is alive.
bool isLive(Cell *inspect);

/// @}
#endif
