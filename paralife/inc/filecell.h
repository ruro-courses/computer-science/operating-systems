#ifndef _FILECELL_H
#define _FILECELL_H

#include "liferules.h"

#include <stdbool.h>
#include <stdio.h>

/// @addtogroup filecell
/// @{

/// Load the next cell from file `file`.
Cell file2cell(FILE *file);

/// Store the cell `data` to file `file`, add a newline if `lineend` is true.
void cell2file(FILE *file, bool lineend, Cell data);

/// @}
#endif
