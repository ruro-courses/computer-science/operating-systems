#ifndef _PACEMAKER_H
#define _PACEMAKER_H

#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>

/// @addtogroup pacemaker
/// @{

/// @brief Possible action values.
///
/// Possible action values, that the workers and pacemaker can perform.
/// @note The `__packed__` attribute tries to enforce `char`-sized enums.
typedef enum __attribute__ ((__packed__))
{
    /// Stop all the piece processes and then the pacemaker. 
    PACE_STOP,  
    /// Load the board from disk.
    PACE_LOAD,
    /// Save the current board state to disk.
    PACE_SAVE,  
    /// Print the current board state.
    /// See @ref termbrd.
    PACE_TERM,
    /// Switch to interactive pacing.
    /// See @ref pacemaker.
    PACE_INTR,
    /// Don't do anything.
    PACE_NOOP,
    /// Compute one iteration on every piece of the board.
    PACE_STEP
} PaceAction;

/// The structure, representing the process, that synchronizes @ref Segment.
typedef struct
{
    /// The name of the PaceMaker.
    char *paceName;
    /// The IPC key used by the PaceMaker.
    key_t paceKey;
    /// The semid, used for synchronization.
    int semPaceSync;
    /// The shmid, used for sending instructions to @ref Segment.
    int shmPaceAction;
    /// The number of @ref Segment, that follow this PaceMaker.
    ssize_t followers;
    /// See @ref Board.segSize.
    ssize_t segSize;
    /// See @ref Board.segNum.
    ssize_t segNum;
    /// The next action, that the segments should perform.
    PaceAction *nextAction;
} PaceMaker;

/// @brief Create a @ref PaceMaker, assigned to file.
///
/// Create a @ref PaceMaker for the @ref Board `boardName`, with the name
/// `paceName`.
///
/// @param boardName The path to the @ref Board directory.
/// @param paceName The @ref PaceMaker filename.
/// @return Returns a pointer to the created @ref PaceMaker.
PaceMaker *createPaceMaker(const char *boardName, const char *paceName)
    __attribute__ ((nonnull));


/// @brief Delete a @ref PaceMaker.
///
/// Deallocates all the resources, that were obtained by @ref createPaceMaker.
/// @warning Not deleting a @ref PaceMaker will result in inability to load
///     this board again untill a reboot is performed, clearing all IPC records.
///
/// @param pMaker The pointer to a previously created @ref PaceMaker.
void deletePaceMaker(PaceMaker *pMaker)
    __attribute__ ((nonnull));


/// @brief Start the @ref PaceMaker.
///
/// Executes the `pMaker` @ref PaceMaker.
/// Waits, while there are actions left to complete, gives out tasks to the
/// segment processes based on the `actions` array, whenever they are ready.
///
/// @param pMaker the @ref PaceMaker to run.
/// @param actions the array of @ref PaceAction, the segments should perform.
void runPaceMaker(PaceMaker *pMaker, PaceAction *actions)
    __attribute__ ((nonnull));

/// Obtain the next Action the board should perform from the user in
/// @ref PACE_INTR mode.
PaceAction getUserAction(void);

/// Create a new PaceMaker file, ask user for board settings for this file.
void newPaceMakerFile(int fd, const char *boardname);

/// @}
#endif
