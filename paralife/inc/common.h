#ifndef _COMMON_H
#define _COMMON_H

#include <stdbool.h>

/// @addtogroup common
/// @{

/// Semaphore id, responsible for detecting a full queue
#define SEM_QUEUE 0

/// Semaphore id, responsible for synchro of task updates
#define SEM_READY 1

/// Wait for the value of the semaphore `semnum` to become 0.
void waitForZero(int semid, unsigned short semnum);

/// Try to change the value of semaphore `semnum` by `N` and wait, if this is
/// not possible.
void waitForNChange(int semid, unsigned short semnum, short N);

/// Kill all associated processes, keeps the calling process alive, if
/// `keepSelf` is true.
void killAll(bool keepSelf);

/// Add the handler for ctrl+C
void handleCtrlC(void);

/// @}
#endif
