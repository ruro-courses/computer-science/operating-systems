#ifndef _SEGCTL_H
#define _SEGCTL_H

#include "liferules.h"

#include <sys/types.h>
#include <sys/ipc.h>

/// @addtogroup segctl
/// @{

/// The structure, representing a segment of the @ref Board, of size
/// [@ref Segment.segSize ✕ @ref Segment.segSize] and the associated process.
typedef struct
{
    /// The name of the board.
    char *segName;
    /// The IPC key used by the PaceMaker.
    key_t segKey;
    /// The shmid, used for exchaning @ref Cell data between
    /// neighbour @ref Segment.
    int shmSegEdge;
    /// See @ref Board.segSize
    ssize_t segSize;
    /// The position of this segment in the @ref Board.
    ssize_t segX;
    /// The position of this segment in the @ref Board.
    ssize_t segY;
    /// The pointer to the shared memory used for exchaning @ref Cell data
    /// between neighbour @ref Segment.
    Cell *edge; 
} Segment;

/// Load the segment with name `segName` from board with name `boardName`.
Segment *createSegment(const char *boardName, const char *segName,
        ssize_t segSize)
    __attribute__ ((nonnull));

/// Remove the segment (from memory)
void deleteSegment(Segment *seg)
    __attribute__ ((nonnull));

/// Run the segment process, that interacts with the @ref PaceMaker.
void runSegment(Segment *seg)
    __attribute__ ((nonnull));

/// Read the values, stored on the disk to segment.
void loadSegment(Segment *seg)
    __attribute__ ((nonnull));

/// Write the values, associated with the segment to disk.
void saveSegment(Segment *seg)
    __attribute__ ((nonnull));

/// Calculate the values for the next step, based on current segment state and
/// store them in `local` storage.
void stepSegment(Segment *seg, Cell *local)
    __attribute__ ((nonnull));

/// Store the values from buffer `local` to correct memory locations.
void updateSegment(Segment *seg, Cell *local)
    __attribute__ ((nonnull));

/// Obtain the pointer to the Cell, located at position `X:Y` in 
/// segment `segX:segY`, adds automatic correction to coords, if needed.
Cell *atSegCoord(ssize_t segX, ssize_t segY, ssize_t X, ssize_t Y);

/// Storage for the currently loaded segment.
extern Cell *curSegment;

/// @}
#endif
