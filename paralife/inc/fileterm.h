#ifndef _FILETERM_H
#define _FILETERM_H

#include "brdctl.h"

/// @addtogroup fileterm
/// @{

/// Output the contents of the file, associated with the board `brd` to terminal.
/// Adds nice formatting with escape sequences and (optional) Unicode characters.
void file2term(Board *brd);

/// @}
#endif
