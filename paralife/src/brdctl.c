#include "brdctl.h"
#include "pacemaker.h"
#include "errctl.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

/// Read and Write permissions for All users.
#define S_IRWA S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH

/// PaceMaker filename
#define PACE_NAME "pacemaker"

/// Segment filename format
#define SEG_NFMT "%02zd-%02zd"

/// Filter out the pacemaker file from directory.
int filterPaceMaker(const struct dirent *file)
{
    if (!strcmp(file->d_name, PACE_NAME) ||
            !strcmp(file->d_name, ".") ||
            !strcmp(file->d_name, ".."))
        return 0;
    return 1;
}

Board *curBoard = NULL;

Board *createBoard(const char *boardName)
{
    // Filename
    char *name = strdup(boardName);
    // Directory
    if (mkdir(name, S_IRWA | S_IXUSR | S_IXGRP | S_IXOTH) && errno != EEXIST)
        error(true, errno, "Couldn't create Board '%s'.\n"
                "mkdir failed.", name);
    struct stat boardStat;
    if (stat(name, &boardStat))
        error(true, errno, "Couldn't create Board '%s'.\n"
                "Couldn't stat the Board.", name);
    if (!S_ISDIR(boardStat.st_mode))
        error(true, errno, "Couldn't create Board '%s'.\n"
                "Board should be a directory.", name);
    // PaceMaker
    PaceMaker *pMaker = createPaceMaker(name, PACE_NAME);
    ssize_t segNum = pMaker->segNum;
    ssize_t segSize = pMaker->segSize;
    // Segment Array
    Segment **segArr = calloc(1, sizeof(*segArr)*segNum*segNum);
    if (!segArr)
        error(true, errno, "Couldn't create Board '%s'.\n"
                "Couldn't allocate memory for the Segment Array.", name);
    // Segments
    struct dirent **namelist = NULL;
    ssize_t followers = scandir(name, &namelist, filterPaceMaker, alphasort); 
    if (followers == -1)
        error(true, errno, "Couldn't create Board '%s'.\n"
                "Couldn't scand the board directory.", name);
    if (followers)
        for (ssize_t i = 0; i < followers; i++)
        {
            Segment *segNext = createSegment(name, namelist[i]->d_name, segSize);
            ssize_t x = segNext->segX;
            ssize_t y = segNext->segY;
            segArr[x + y*segNum] = segNext;
            free(namelist[i]);
        }
    else
        for (ssize_t i = 0; i < segNum*segNum; i++)
        {
            ssize_t segNameLen = snprintf(NULL, 0, SEG_NFMT, i/segNum, i%segNum);
            char segName[segNameLen+1];
            snprintf(segName, segNameLen+1, SEG_NFMT, i/segNum, i%segNum);
            Segment *segNext = createSegment(name, segName, segSize);
            segArr[i] = segNext;
        }
    free(namelist);
    pMaker->followers = followers ? followers : segNum*segNum;
    // Memory
    Board *ret = calloc(1, sizeof(*ret));
    if (!ret)
        error(true, errno, "Couldn't create Board '%s'.\n"
                "Couldn't allocate memory for the Board.", name);
    // Save Values
    ret->brdName = name;
    ret->masterPace = pMaker;
    ret->segNum = segNum;
    ret->segSize = segSize;
    ret->segArr = segArr;
    return ret;
}

void deleteBoard(Board *brd)
{
    // Free Resources.
    deletePaceMaker(brd->masterPace);
    ssize_t segNum = brd->segNum;
    for (ssize_t i = 0; i < segNum*segNum; i++)
        if (brd->segArr[i])
            deleteSegment(brd->segArr[i]);
    free(brd->segArr);
    free(brd->brdName);
    free(brd);
}

void runBoard(Board *brd, PaceAction *actions)
{
    // Spawn all the Segments, then prepare the PaceMaker
    curBoard = brd;
    ssize_t segNum = brd->segNum;
    for (ssize_t i = 0; i < segNum*segNum; i++)
        if (brd->segArr[i])
            runSegment(brd->segArr[i]);
    runPaceMaker(brd->masterPace, actions);
}
