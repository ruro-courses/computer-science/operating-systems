#include "filecell.h"
#include "errctl.h"

#include <errno.h>
#include <stdio.h>
#include <ctype.h>

Cell file2cell(FILE *file)
{
    char buf;
    while (fscanf(file, "%c", &buf) == 1)
        switch (buf)
        {
            case 'W':
                return CE_WALL;
            case 'F':
                return CE_FADE;
            case 'D':
                return CE_DEAD;
            case 'B':
                return CE_BORN;
            case 'L':
                return CE_LIVE;
            default:
                if (!isspace(buf))
                    error(false, 0, "Unable to parse the Segment file.\n"
                            "Stray '%c' in file.", buf);
        }
    return CE_WALL;
}

void cell2file(FILE *file, bool lineend, Cell data)
{
    char buf = 'W';
    switch (data)
    {
        case CE_WALL:
            buf = 'W';
            break;
        case CE_FADE:
            buf = 'F';
            break;
        case CE_DEAD:
            buf = 'D';
            break;
        case CE_BORN:
            buf = 'B';
            break;
        case CE_LIVE:
            buf = 'L';
            break;
        default:
            error(true, errno, "Broken cell with value #%d in filecell.", data);
    }
    fprintf(file, "%c%c", buf, lineend ? '\n' : ' ');
}
