#include "brdctl.h"
#include "pacemaker.h"

#include <stdio.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Usage: %s <board name>\n", argv[0]);
        return 0;
    }
    Board *brd = createBoard(argv[1]);
    runBoard(brd, (PaceAction[]) { PACE_LOAD, PACE_STEP, PACE_SAVE, PACE_STOP });
    deleteBoard(brd);
}
