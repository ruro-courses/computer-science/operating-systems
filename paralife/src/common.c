#include "common.h"
#include "errctl.h"

#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/sem.h>

void waitForZero(int semid, unsigned short semnum)
{
    struct sembuf op =
    {
        .sem_num = semnum,
        .sem_op = 0,
        .sem_flg = 0
    };
    while (semop(semid, &op, 1))
        if (errno != EINTR)
            error(true, errno, "Couldn't wait for zero in Common."
                    "Semaphore #%d failed.", semnum);
}

void waitForNChange(int semid, unsigned short semnum, short N)
{
    struct sembuf op =
    {
        .sem_num = semnum,
        .sem_op = N,
        .sem_flg = 0
    };
    while (semop(semid, &op, 1))
        if (errno != EINTR)
            error(true, errno, "Couldn't raise by %d in Common."
                    "Semaphore #%d failed.", N, semnum);
}

/// SIGINT handler for CtrlC intercept
void intKillsAll(int sig)
{
    (void)sig;
    killAll(false);
}

void handleCtrlC(void)
{
    // Add the SIGINT handler.
    sigset_t maskAll;
    sigfillset(&maskAll);
    struct sigaction act =
    {
        .sa_handler = intKillsAll,
        .sa_mask = maskAll,
        .sa_flags = SA_RESTART
    };
    if (sigaction(SIGINT, &act, NULL))
        error(errno, true, "Couldn't change signal handler in Common.");
}

volatile sig_atomic_t SigIntRecieved = 0;

void catchSigInt(int sig)
{
    (void)sig; // Unused parameter
    SigIntRecieved = 1;
}

void killAll(bool keepSelf)
{
    sigset_t maskSigInt, maskOld;
    struct sigaction oact;
    if (keepSelf)
    {
        // Kill everybody without killing ourselves.
        if (sigemptyset(&maskSigInt) ||
                sigaddset(&maskSigInt, SIGINT))
            error(errno, true, "Couldn't modify signal set in Common.");
        // Add the SIGINT handler.
        struct sigaction act =
        {
            .sa_handler = catchSigInt,
            .sa_mask = maskSigInt,
            .sa_flags = SA_RESTART
        };
        if (sigaction(SIGINT, &act, &oact))
            error(errno, true, "Couldn't change signal handler in Common.");
        // Protect ourselves from us.
        if (sigprocmask(SIG_BLOCK, &maskSigInt, &maskOld))
            error(errno, true, "Couldn't change signal process mask in Common.");
    }
    // Send everybody a SIGINT
    if (kill(0, SIGINT))
        error(errno, true, "Couldn't send the kill signal in Common.");
    if (keepSelf)
    {
        // Catch it ourselves safely.
        while (!SigIntRecieved)
            sigsuspend(&maskOld);
        // Restore SIGINT power.
        if (sigprocmask(SIG_UNBLOCK, &maskSigInt, &maskOld))
            error(errno, true, "Couldn't change signal process mask in Common.");
        if (sigaction(SIGINT, &oact, NULL))
            error(errno, true, "Couldn't change signal handler in Common.");
    }
    // Wait for the killed to terminate.
    while (wait(NULL) != -1 && errno != ECHILD);
    if (!keepSelf)
        exit(0);
}
