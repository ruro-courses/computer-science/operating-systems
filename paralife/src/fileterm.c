#include "fileterm.h"
#include "filecell.h"
#include "errctl.h"

#include <stdio.h>
#include <errno.h>
#include <stdbool.h>

/// Clear formatting
#define CLEAR "\e[0m"
/// Set foreground color
#define SETFG(x) "\e[38;5;"#x"m"
/// Set background color
#define SETBG(x) "\e[48;5;"#x"m"

/// Define ASCII at compile time to enable ASCII-compatible mode,
/// the default mode uses Unicode.
#ifndef ASCII
    #define WALL_FMT SETFG(15) SETBG(15) ///< White on White
    #define DEAD_FMT SETFG( 0) SETBG( 0) ///< Black on Black
    #define LIVE_FMT SETFG( 2) SETBG( 2) ///< Green on Green
    #define FADE_FMT SETFG( 1) SETBG( 0) ///< Red on Black
    #define BORN_FMT SETFG( 0) SETBG( 2) ///< Black on Green
    #define WALL_STR "██"
    #define DEAD_STR "  "
    #define LIVE_STR "██"
    #define FADE_STR "××"
    #define BORN_STR "··"
#else
    #define WALL_FMT SETFG( 0) SETBG(15) ///< Black on White
    #define DEAD_FMT SETFG( 0) SETBG( 0) ///< White on Black
    #define LIVE_FMT SETFG( 2) SETBG( 0) ///< Green on Black
    #define FADE_FMT SETFG( 1) SETBG( 0) ///< Red on Black
    #define BORN_FMT SETFG(10) SETBG( 0) ///< Light Green on Black
    #define WALL_STR "##"
    #define DEAD_STR "  "
    #define LIVE_STR "@@"
    #define FADE_STR "--"
    #define BORN_STR "@@"
#endif

#define CE_CASE(x)                                  \
    case CE_##x:                                    \
        printf(CLEAR x##_FMT x##_STR CLEAR);        \
        break

void file2term(Board *brd)
{
    ssize_t segSize = brd->segSize;
    ssize_t segNum = brd->segNum;
    for (ssize_t segY = 0; segY < segNum; segY++)
    {
        FILE *segRow[segNum];
        for (ssize_t segX = 0; segX < segNum; segX++)
            if (!(segRow[segX] = fopen(brd
                            ->segArr[segX + segY*segNum]
                            ->segName, "r")))
                error(true, errno, "Couldn't open file '%s'.\n",
                        brd->segArr[segX + segY*segNum]->segName);
        for (ssize_t y = 0; y < segSize; y++)
        {
            for (ssize_t segX = 0; segX < segNum; segX++)
                for (ssize_t x = 0; x < segSize; x++)
                {
                    Cell c = file2cell(segRow[segX]);
                    switch (c)
                    {
                        CE_CASE(WALL);
                        CE_CASE(BORN);
                        CE_CASE(LIVE);
                        CE_CASE(FADE);
                        CE_CASE(DEAD);
                    default:
                        error(true, errno, "Broken cell with value #%d in fileterm.", c);
                    }
                    fflush(stdout);
                }
            printf("\n");
            fflush(stdout);
        }
        for (ssize_t segX = 0; segX < segNum; segX++)
            fclose(segRow[segX]);
    }
}
