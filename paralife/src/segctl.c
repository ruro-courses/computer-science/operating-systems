#include "segctl.h"
#include "brdctl.h"
#include "common.h"
#include "errctl.h"
#include "filecell.h"

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/shm.h>

/// Read and Write permissions for All users.
#define S_IRWA S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH

Cell *curSegment = NULL;

Segment *createSegment(const char *boardName, const char *segName,
        ssize_t segSize)
{
    // Filename
    ssize_t nameLen = snprintf(NULL, 0, "%s/%s", boardName, segName);
    char *name = calloc(1, nameLen+1);
    if (!name)
        error(true, errno, "Couldn't create Segment '%s/%s'.\n"
                "Couldn't allocate memory for the filename.",
                boardName, segName);
    snprintf(name, nameLen+1, "%s/%s", boardName, segName);
    // Position
    size_t segX = 0;
    size_t segY = 0;
    if (sscanf(segName, "%zd-%zd", &segY, &segX) != 2)
        error(true, errno, "Couldn't create Segment '%s'.\n"
                "The Segment filename is malformed.", name);
    // File
    int segFile = open(name, O_CREAT | O_RDWR, S_IRWA);
    if (segFile == -1)
        error(true, errno, "Couldn't create Segment '%s'.\n"
                "The board may already have that Segment.", name);
    // Key
    key_t segKey = ftok(name, 'S');
    if (segKey == -1)
        error(true, errno, "Couldn't create Segment '%s'.\n"
                "Couldn't obtain key after file creation.", name);
    close(segFile);
    // Shared memory
    int shmid = shmget(segKey, 4*(segSize-1)*sizeof(Cell),
            IPC_CREAT | IPC_EXCL | S_IRWA);
    if (shmid == -1)
        error(true, errno, "Couldn't create Segment '%s'.\n"
                "The board may already have this Segment.", name);
    Cell *shm = shmat(shmid, NULL, 0);
    if (shm == (void *)-1)
        error(true, errno, "Couldn't create Segment '%s'.\n"
                "Couldn't attach the shared memory.", name);
    if (shmctl(shmid, IPC_RMID, 0))
        error(true, errno, "Couldn't create Segment '%s'.\n"
                "Couldn't release the shared memory, the Segment is "
                "most likely broken.", name);
    // Memory
    Segment *ret = calloc(1, sizeof(*ret));
    if (!ret)
        error(true, errno, "Couldn't create Segment '%s'.\n"
                "Couldn't allocate memory for the Segment.", name);
    // Save values
    ret->segName = name;
    ret->segKey = segKey;
    ret->shmSegEdge = shmid;
    ret->segX = segX;
    ret->segY = segY;
    ret->segSize = segSize;
    ret->edge = shm;
    return ret;
}

void deleteSegment(Segment *seg)
{
    // Free resources
    shmdt(seg->edge);
    free(seg->segName);
    free(seg);
}

Cell *atSegCoord(ssize_t segX, ssize_t segY, ssize_t X, ssize_t Y)
{
    ssize_t segSize = curBoard->segSize;
    ssize_t segNum = curBoard->segNum;
    // Outsife of Board
    if (segX < 0 || segNum-1 < segX || segY < 0 || segNum-1 < segY)
        return NULL;
    // Outside of Segment
    if (X < 0 || segSize-1 < X || Y < 0 || segSize-1 < Y)
    {
        segX -= (X < 0);
        segY -= (Y < 0);
        segX += (segSize-1 < X);
        segY += (segSize-1 < Y);
        X += segSize;
        Y += segSize;
        X %= segSize;
        Y %= segSize;
        return atSegCoord(segX, segY, X, Y);
    }
    // Internals
    if (0 != X && X != segSize-1 && 0 != Y && Y != segSize-1)
        return &curSegment[X + Y*segSize];
    // Upper and Right Edges
    if (Y == 0 || X == segSize-1)
        return &curBoard
            ->segArr[segX + segY*segNum]
            ->edge[X + Y];
    // Lower and Left Edges
    if (X == 0 || Y == segSize-1)
        return &curBoard
            ->segArr[segX + segY*segNum]
            ->edge[2*(segSize-1) + (segSize-1-X) + (segSize-1-Y)];
    error(true, errno, "Couldn't get segmented coordinates for Board '%s'.\n"
            "segX:%zd segY:%zd X:%zd Y:%zd", curBoard->brdName, segX, segY, X, Y);
}

void runSegment(Segment *seg)
{
    pid_t p = fork();
    if (p == -1)
        error(true, errno, "Couldn't run Segment '%s'.\n"
                "Couldn't fork the process.", seg->segName);
    if (!p)
    {
        ssize_t segSize = seg->segSize;
        curSegment = calloc(segSize*segSize, sizeof(Cell));
        if (!curSegment)
            error(false, errno, "Couldn't run Segment '%s'.\n"
                    "Couldn't allocate memory for the curSegment.", seg->segName);
        int semid = curBoard->masterPace->semPaceSync;
        PaceAction *next = curBoard->masterPace->nextAction;
        Cell local[segSize*segSize];
        bool needsUpdate = false;
        while (1)
        {
            // Queue up for next task.
            waitForNChange(semid, SEM_QUEUE, -1);
            // Wait until everybody has queued up for next task.
            waitForZero(semid, SEM_QUEUE);
            // Nobody is working. Quick, update the values! 
            if (needsUpdate)
                updateSegment(seg, local);
            needsUpdate = false;
            // Wait in queue to recieve task.
            waitForNChange(semid, SEM_READY, -1);
            // Wait until everybody is ready to recieve task.
            waitForZero(semid, SEM_READY);
            // Complete next task.
            switch (*next)
            {
                case PACE_STEP:
                    stepSegment(seg, local);
                    needsUpdate = true;
                    break;
                case PACE_SAVE:
                    saveSegment(seg);
                    break;
                case PACE_LOAD:
                    loadSegment(seg);
                    break;
                default:
                    error(false, 0, "Couldn't run Segment '%s'.\n"
                            "Unknown PaceAction #%d.", seg->segName, *next);
            }
        }
    }
}

void loadSegment(Segment *seg)
{
    FILE *file = fopen(seg->segName, "r");
    if (!file)
        error(true, errno, "Couldn't load Segment '%s'.\n"
                "Unable to open Segment file.", seg->segName);
    ssize_t segSize = seg->segSize;
    ssize_t segX = seg->segX;
    ssize_t segY = seg->segY;
    for (ssize_t Y = 0; Y < segSize; Y++)
        for (ssize_t X = 0; X < segSize; X++)
            *atSegCoord(segX, segY, X, Y) = file2cell(file);
    fflush(file);
    fclose(file);
}

void saveSegment(Segment *seg)
{
    FILE *file = fopen(seg->segName, "w");
    if (!file)
        error(true, errno, "Couldn't save Segment '%s'.\n"
                "Unable to open Segment file.", seg->segName);
    ssize_t segSize = seg->segSize;
    ssize_t segX = seg->segX;
    ssize_t segY = seg->segY;
    for (ssize_t Y = 0; Y < segSize; Y++)
        for (ssize_t X = 0; X < segSize; X++)
        {
            Cell *next = atSegCoord(segX, segY, X, Y);
            cell2file(file, (X == segSize-1), *next);
        }
    fflush(file);
    fclose(file);
}

void stepSegment(Segment *seg, Cell *local)
{
    ssize_t segSize = seg->segSize;
    ssize_t segX = seg->segX;
    ssize_t segY = seg->segY;
    for (ssize_t Y = 0; Y < segSize; Y++)
        for (ssize_t X = 0; X < segSize; X++)
        {
            ssize_t surr = 0;
            surr += isLive(atSegCoord(segX, segY, X-1, Y-1));
            surr += isLive(atSegCoord(segX, segY, X-1, Y));
            surr += isLive(atSegCoord(segX, segY, X-1, Y+1));
            surr += isLive(atSegCoord(segX, segY, X, Y-1));
            surr += isLive(atSegCoord(segX, segY, X, Y+1));
            surr += isLive(atSegCoord(segX, segY, X+1, Y-1));
            surr += isLive(atSegCoord(segX, segY, X+1, Y));
            surr += isLive(atSegCoord(segX, segY, X+1, Y+1));
            local[X + segSize*Y] = getNextStep(*atSegCoord(segX, segY, X, Y),
                    surr);
        }
}

void updateSegment(Segment *seg, Cell *local)
{
    ssize_t segSize = seg->segSize;
    ssize_t segX = seg->segX;
    ssize_t segY = seg->segY;
    for (ssize_t Y = 0; Y < segSize; Y++)
        for (ssize_t X = 0; X < segSize; X++)
            *atSegCoord(segX, segY, X, Y) = local[X + segSize*Y];
}
