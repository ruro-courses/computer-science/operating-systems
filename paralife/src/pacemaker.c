#include "pacemaker.h"
#include "brdctl.h"
#include "common.h"
#include "errctl.h"
#include "fileterm.h"

#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <sys/shm.h>

/// Read and Write permissions for All users.
#define S_IRWA S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                (Linux-specific) */
};

PaceMaker *createPaceMaker(const char *boardName, const char *paceName)
{
    // Filename
    ssize_t nameLen = snprintf(NULL, 0, "%s/%s", boardName, paceName);
    char *name = calloc(1, nameLen+1);
    if (!name)
        error(true, errno, "Couldn't create PaceMaker '%s/%s'.\n"
                "Couldn't allocate memory for the filename.",
                boardName, paceName);
    snprintf(name, nameLen+1, "%s/%s", boardName, paceName);
    // File
    int paceFile = open(name, O_CREAT | O_RDWR | O_EXCL, S_IRWA);
    if (paceFile != -1)
        newPaceMakerFile(paceFile, boardName);
    else if (errno != EEXIST)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't create the PaceMaker file.", name);
    paceFile = open(name, O_RDWR, S_IRWA);
    if (paceFile == -1)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't open the PaceMaker file.", name);
    // Key
    key_t paceKey = ftok(name, 'P');
    if (paceKey == -1)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't obtain key after file creation.", name);
    // Size and number of segments info
    ssize_t segSize = 0;
    ssize_t segNum = 0;
    FILE *paceContents = fdopen(paceFile, "r");
    if (!paceContents)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't read the PaceMaker file.", name);
    if (fscanf(paceContents, "%zd %zd", &segSize, &segNum) != 2)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "The PaceMaker file is malformed.", name);
    fclose(paceContents);
    // Shared memory
    int shmid = shmget(paceKey, sizeof(PaceAction), IPC_CREAT | IPC_EXCL | S_IRWA);
    if (shmid == -1)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "The board may already have a PaceMaker (shm).", name);
    void *shm = shmat(shmid, NULL, 0);
    if (!shm)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't attach the shared memory.", name);
    if (shmctl(shmid, IPC_RMID, 0))
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't release the shared memory, the PaceMaker is "
                "most likely broken.", name);
    // Semaphores
    int semid = semget(paceKey, 2, IPC_CREAT | S_IRWA);
    if (semid == -1)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "The board may already have a PaceMaker (sem).", name);
    if (semctl(semid, 0, SETVAL, (union semun){ .val = 0 }) ||
            semctl(semid, 1, SETVAL, (union semun){ .val = 0 }))
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't initialize the semaphores.", name);
    // Memory
    PaceMaker *ret = calloc(1, sizeof(*ret));
    if (!ret)
        error(true, errno, "Couldn't create PaceMaker '%s'.\n"
                "Couldn't allocate memory for the PaceMaker.", name);
    // Save values
    ret->followers = 0;
    ret->segSize = segSize;
    ret->segNum = segNum;
    ret->paceName = name;
    ret->paceKey = paceKey;
    ret->semPaceSync = semid;
    ret->shmPaceAction = shmid;
    ret->nextAction = shm;
    return ret;
}

void deletePaceMaker(PaceMaker *pMaker)
{
    semctl(pMaker->semPaceSync, 0, IPC_RMID);
    shmdt(pMaker->nextAction);
    free(pMaker->paceName);
    free(pMaker);
}

void newPaceMakerFile(int fd, const char *boardname)
{
    printf("The board '%s' doesn't exist. Creating...\n"
            "Please input the size of a Segment and the"
            " width of the Board in segments:\n", boardname);
    fflush(stdout);
    ssize_t segSize = 0;
    ssize_t segNum = 0;
    if (scanf("%zd %zd", &segSize, &segNum) != 2)
        error(true, errno, "Couldn't create PaceMaker for board '%s'.\n"
                "User input not recognized.", boardname);
    if (segNum < 1)
        error(true, 0, "There should be at least 1 segment on the board.");
    if (segNum > 99)
        error(true, 0, "At most 99 segments wide boards are allowed.");
    if (segSize < 2)
        error(true, 0, "A Segment should be at least 2 cells wide.");
    FILE *paceContents = fdopen(fd, "w");
    if (!paceContents)
        error(true, errno, "Couldn't create PaceMaker for board '%s'.\n"
                "Couldn't initialize the PaceMaker file.", boardname);
    fprintf(paceContents, "%zd %zd\n", segSize, segNum);
    fflush(paceContents);
    fclose(paceContents);
}

PaceAction getUserAction(void)
{
    static bool nextHelp = true;
    static bool termEnabled = true;
    static bool nextTerm = false;
    static size_t stepsLeft = 0;
    if (stepsLeft)
    {
        stepsLeft--;
        return PACE_STEP;
    }
    if (nextHelp)
    {
        nextHelp = false;
        printf("[h]elp (this message) | [q]uit | [s]ave | [l]oad\n"
                "[t]oggle output to terminal\n"
                "[0-9] perform 2^n steps\n");
    }
    if (nextTerm)
    {
        nextTerm = false;
        return PACE_TERM;
    }
    nextTerm = termEnabled;
    char c = '\0';
    while (scanf("%c", &c) == 1 && isspace(c))
        c = '\0';
    switch (c)
    {
        case 'h':
            nextHelp = true;
            return PACE_NOOP;
        case 's':
            return PACE_SAVE;
        case 'l':
            return PACE_LOAD;
        case 't':
            termEnabled = termEnabled ? false : true;
            nextTerm = termEnabled;
            return PACE_NOOP;
        case 'q':
        case '\0':
            return PACE_STOP;
        default:
            if (isdigit(c))
                stepsLeft = 1 << (c-'0');
            else
                printf("Unknown command '%c'. Press [h] for help.\n", c);
            return PACE_NOOP;
    }
}

void runPaceMaker(PaceMaker *pMaker, PaceAction *actions)
{
    int followers = pMaker->followers;
    int semid = pMaker->semPaceSync;
    // Let everybody start queueing up for tasks.
    waitForNChange(semid, SEM_QUEUE, followers);
    // Wait until everybody has queued up for next task.
    waitForZero(semid, SEM_QUEUE);
    for (ssize_t i = 0; actions[i] != PACE_STOP; i++)
    {
        // Load new action.
        PaceAction next = actions[i];
        if (next == PACE_INTR)
        {
            i--;
            next = getUserAction();
        }
        if (next == PACE_STOP)
            break;
        if (next == PACE_NOOP)
            continue;
        if (next == PACE_TERM)
            *pMaker->nextAction = PACE_SAVE;
        else
            *pMaker->nextAction = next;
        // Let everybody take up the next task.
        waitForNChange(semid, SEM_READY, followers);
        // Wait for everybody to finish taking the new task.
        waitForZero(semid, SEM_READY);
        // Let everybody start queueing up for tasks.
        waitForNChange(semid, SEM_QUEUE, followers);
        // Wait until everybody has queued up for next task.
        waitForZero(semid, SEM_QUEUE);
        if (next == PACE_TERM)
            file2term(curBoard);
    }
    killAll(true);
}
