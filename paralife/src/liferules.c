#include "liferules.h"
#include "errctl.h"

#include <errno.h>

Cell getNextStep(Cell self, ssize_t surr)
{
    switch  (self)
    {
        case CE_WALL:
            return CE_WALL;
        case CE_BORN:
        case CE_LIVE:
            if (2 <= surr && surr <= 3)
                return CE_LIVE;
            else
                return CE_FADE;
        case CE_FADE:
        case CE_DEAD:
            if (surr != 3)
                return CE_DEAD;
            else
                return CE_BORN;
        default:
            error(true, errno, "Broken cell with value #%d in liferules.", self);
            return CE_WALL;
    }
}

bool isLive(Cell *inspect)
{
    return inspect && (*inspect == CE_LIVE || *inspect == CE_BORN);
}
