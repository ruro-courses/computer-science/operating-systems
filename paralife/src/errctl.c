#include "errctl.h"
#include "common.h"

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

void error(bool master, int code, const char *fmt, ...)
{
    // Print the formatted message.
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

    fprintf(stderr, "\n\n");
    // Print the error message, if any.
    if (code)
        fprintf(stderr, "Error: %s \n", strerror(code));
    fflush(stderr);

    if (!master)
    {
        fprintf(stderr, "An error was detected in a child process.\n"
                "To terminate execution, please press Ctrl-C.\n");
        while (pause());
    }
    killAll(false);
    exit(0);
}
