#include "sigcounter.h"
#include "terminator.h"

#include <signal.h>

int main(void)
{

    EventList e = {
        {SIGALRM, 10}, // Wait for SIGALRM to happen 10 times.
        {SIGWINCH, 3}, // Wait for SIGWINCH to happen 3 times.
        {0, 0}         // "NULL" - terminator.
    };
    announce("Waiting for signals SIGALRM(%d) and SIGWINCH(%d).\n",
                                  SIGALRM,        SIGWINCH);
    waitEL(e);
    announce("All signals recieved.\n");
}
