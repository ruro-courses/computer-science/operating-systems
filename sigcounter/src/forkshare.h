#ifndef _FORKSHARE_H
#define _FORKSHARE_H

ssize_t d_pagesize(void);

void *forkshare(size_t)
    __attribute__ ((alloc_size(1)))
    __attribute__ ((malloc));

#endif
