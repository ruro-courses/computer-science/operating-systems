#include "sigcounter.h"
#include "terminator.h"

#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

bool isELempty(EventList e)
{
    // Check if no Non-Zero events are left in EventList e.
    for (size_t i = 0; e[i].signo; i++)
        if (e[i].count)
            return false;
    return true;
}

void tick(EventList e, int sig)
{
    // Decrement the counter for signal sig in EventList e.
    for (size_t i = 0; e[i].signo; i++)
        if (e[i].signo == sig)
        {
            e[i].count -= !!e[i].count;
            announce("Recieved signal %d."
                    " Waiting for %zu more signals of this type.\n",
                    sig, e[i].count);
            return;
        }
}

void waitEL(EventList e)
{
    // Make a new signal mask from EventList.
    sigset_t old, cur;
    if (sigemptyset(&cur))
        terminate(errno, "Couldn't empty the set.\n");
    for (size_t i = 0; e[i].signo; i++)
        if (sigaddset(&cur, e[i].signo))
            terminate(errno, "Couldn't add signal to the set.\n");

    // Block the signals in the mask.
    if (sigprocmask(SIG_BLOCK, &cur, &old))
        terminate(errno, "Couldn't change signal mask.\n");

    // Wait for the signals.
    int sig;
    while (!isELempty(e))
    {
        while ((sig = sigwaitinfo(&cur, NULL)) == -1)
            if (errno != EINTR)
                terminate(errno, "Couldn't wait for required signals.\n");
        tick(e, sig);
    }

    // Restore the old signal mask.
    if (sigprocmask(SIG_SETMASK, &old, NULL))
        terminate(errno, "Couldn't restore the signal mask.\n");
}
