#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/types.h>

#include "terminator.h"
#include "forkshare.h"

sem_t *term_sem = NULL;

void atomize(void)
{
    if (term_sem)
        return;
    
    // Make a semaphore for I/O operations.
    term_sem = forkshare(sizeof(*term_sem));
    if (!term_sem)
        terminate(errno, "Unable to allocate memory for semaphore.\n");
        
    if (sem_init(term_sem, 1, 1))
        terminate(errno, "Unable to initialize the semaphore.\n");
}

void announce(const char *fmt, ...)
{
    // If term_sem is initialized, use semaphores for atomic I/O.
    if (term_sem)
        while (sem_wait(term_sem))
            if (errno != EINTR)
                exit(-1);

    // Print the formatted message.
    va_list args;
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);
    fflush(stdout);

    // If term_sem is initialized, use semaphores for atomic I/O.
    if (term_sem)
        sem_post(term_sem);
}

void terminate(int code, const char *fmt, ...)
{
    // If term_sem is initialized, use semaphores for atomic I/O.
    if (term_sem)
        while (sem_wait(term_sem))
            if (errno != EINTR)
                exit(-1);

    // Print the formatted message.
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

    // Print the error message, if any.
    if (code)
        fprintf(stderr, "Error: %s \n", strerror(code));
    fflush(stderr);

    // If term_sem is initialized, use semaphores for atomic I/O.
    if (term_sem)
        sem_post(term_sem);

    kill(0, SIGINT);
    exit(-errno);
}
