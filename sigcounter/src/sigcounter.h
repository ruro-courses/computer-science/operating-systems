#ifndef _SIGCOUNTER_H
#define _SIGCOUNTER_H

#include <stdlib.h>

typedef struct
{
    int signo;
    size_t count;
} Event;

typedef Event EventList[];

void waitEL(EventList) __attribute__(( nonnull ));

#endif
