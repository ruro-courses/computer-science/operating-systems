#ifndef _TERMINATOR_H
#define _TERMINATOR_H

void atomize(void);

void announce(const char *, ...)
    __attribute__ ((nonnull))
    __attribute__ ((format (printf, 1, 2)));

void terminate(int, const char *, ...)
    __attribute__ ((noreturn))
    __attribute__ ((nonnull))
    __attribute__ ((format (printf, 2, 3)));

#endif
