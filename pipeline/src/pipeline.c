#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <fcntl.h>

#include "pipeline.h"
#include "terminator.h"

Pipe mkPipe(void)
{
    // Create the pipe.
    int fd[2];
    if (pipe(fd))
        terminate(errno, "Couldn't spawn a pipe.\n");
    // Make it close on exec.
    if (fcntl(fd[0], F_SETFD, FD_CLOEXEC) == -1 ||
        fcntl(fd[1], F_SETFD, FD_CLOEXEC) == -1)
        terminate(errno, "Couldn't enable close-on-exec for pipe.\n");
    // Make the Pipe object.
    Pipe ret = { .r = fd[0], .w = fd[1] };
    return ret;
}

void spawn(Pipe i, Pipe o, const char *cmd)
{
    pid_t p = fork();
    if (p == -1)
        terminate(errno, "Couldn't fork.\n");
    // Child.
    if (!p)
    {
        // Change stdin and stdout to ends of io Pipe.
        if (dup2(i.r, STDIN_FILENO) == -1 ||
            dup2(o.w, STDOUT_FILENO) == -1)
            terminate(errno, "Couldn't replace IO with with pipe.\n");

        // Run command.
        if (execlp(cmd, cmd, NULL))
            terminate(errno, "Couldn't run command %s.\n", cmd);
    }
}

void pipeline(Pipe ends, const char *proc, ...)
{
    Pipe next, prev = ends;
    va_list cmds;
    const char *cmd;

    // For every argument:
    va_start(cmds, proc);
    while ( (cmd = va_arg(cmds, const char *)) )
    {
        // Run a command in new process.
        next = mkPipe();
        spawn(prev, next, proc);

        // Adjust pipes for next process.
        if (close(prev.r) || close(next.w))
            terminate(errno, "Couldn't close pipe end.\n");
        prev = next;
        proc = cmd;
    }
    va_end(cmds);

    // Run last process.
    spawn(prev, ends, proc);

    // Clean up.
    close(prev.r);
    close(ends.w);
}
