#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "pipeline.h"
#include "terminator.h"

#define S_IRWA S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH

int main(int argc, char *argv[])
{
    // Make terminator IO atomic.
    atomize();

    // Open input and output files.
    int inp = open(argv[2], O_RDONLY | O_CLOEXEC);
    if (inp == -1)
        terminate(errno, "Couldn't open file %s.\n", argv[2]);

    int out = open(argv[4], O_WRONLY | O_CREAT | O_CLOEXEC | O_TRUNC, S_IRWA);
    if (out == -1)
        terminate(errno, "Couldn't open file %s.\n", argv[4]);

    // X < A | Y > B
    Pipe io = {.r = inp, .w = out};
    pipeline(io, argv[1], argv[3], NULL);

    // Wait for children to die.
    while (wait(NULL) != -1);
}
