#ifndef _PIPELINE_H
#define _PIPELINE_H

typedef struct {
    int r;
    int w;
} Pipe;

Pipe mkPipe(void);
void spawn(Pipe, Pipe, const char *) __attribute__ (( nonnull ));
void pipeline(Pipe, const char *, ...)
    __attribute__ (( nonnull(2) ))
    __attribute__ (( sentinel ));

#endif
