#ifndef _SWARM_H
#define _SWARM_H
#include <semaphore.h>
#include <stdbool.h>
#include <limits.h>

typedef struct {
    char filename[PATH_MAX];
    size_t offset;
    size_t amount;
} Task;

typedef struct {
    char **files;
    size_t step;
    size_t cur_file;
    size_t cur_off;
    size_t cur_max;
} TaskStream;

typedef struct {
    Task task;
    sem_t hastask;
    bool keepfile;
    bool alive;
    double result;
    sem_t hasresult;
} Worker;

typedef struct {
    size_t worker_num;
    Worker workers[];
} Swarm;

TaskStream *makeTaskStream(char *[], size_t) __attribute__ (( nonnull ));
int getTask(TaskStream *, Task *) __attribute__ (( nonnull ));
Swarm *makeSwarm(size_t);
void makeWorker(Worker *) __attribute__ (( nonnull ));
void pushTask(Worker *, Task *) __attribute__ (( nonnull ));
int popTask(Worker *, double *) __attribute__ (( nonnull ));
void workLoop(Worker *) __attribute__ (( nonnull ));

#endif
