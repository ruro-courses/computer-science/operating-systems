#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>

#include "forkshare.h"
#include "terminator.h"

void *page = NULL;
ssize_t pagesize = 0;
ssize_t used = 0;

#define P_RW PROT_READ | PROT_WRITE
#define M_SA MAP_SHARED | MAP_ANONYMOUS

ssize_t d_pagesize(void)
{
    if (!pagesize)
    {
        pagesize = sysconf(_SC_PAGESIZE);
        if (pagesize == -1)
            terminate(errno, "Unable to get PAGESIZE.\n");
    }
    return pagesize;
}

void *forkshare(size_t size)
{
    // Whole block allocation.
    if (size > d_pagesize())
    {
        void *ret = mmap(NULL, size, P_RW, M_SA, -1, 0);
        if (ret == MAP_FAILED)
            return NULL;
        return ret;
    }

    // Start new page.
    if (size > pagesize - used)
    {
        page = NULL;
        used = 0;
    }

    // Create a shared memory segment.
    if (!page)
        page = mmap(NULL, pagesize, P_RW, M_SA, -1, 0);

    // Unable to mmap.
    if (page == MAP_FAILED)
        return NULL;

    // Success.
    void *ret = (char *)page + used;
    used += size;
    return ret;
}
