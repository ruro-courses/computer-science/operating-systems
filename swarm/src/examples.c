#include <float.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "swarm.h"
#include "terminator.h"

#define K 32

void stop(int i)
{
    (void) i; /* unused */
    _exit(0);
}

int main(int argc, char *argv[])
{
    // Add signal handler, for safe CTRL+C.
    signal(SIGINT, stop);
    // Make terminator I/O functions atomic.
    atomize();
    // Init swarm of processes.
    Swarm *swarm = makeSwarm(K);
    // Init task stream with default step size.
    TaskStream *tstream = makeTaskStream(&argv[1], 8);
    Task next;
    // Preload swarm with tasks.
    for (size_t i = 0; i < K; i++)
    {
        if (!getTask(tstream, &next))
            break;
        pushTask(&swarm->workers[i], &next);
    }
    double minval = DBL_MAX;
    double tmp;
    bool all_dead;
    while (1)
    {
        // popTask returns -1 if worker is dead,
        //                  0 if worker is done with current task
        //                  1 if worker is working.
        all_dead = true;
        for (size_t i = 0; i < K; i++)
        {
            int ret = popTask(&swarm->workers[i], &tmp);
            if (!ret)
            {
                if (tmp < minval)
                    announce("New minimum found: %.*e\n", DECIMAL_DIG+1, tmp);
                minval = (minval < tmp) ? minval : tmp;
                // Load next task.
                if (getTask(tstream, &next))
                    pushTask(&swarm->workers[i], &next);
                all_dead = false;
            }
            if (ret == 1)
                all_dead = false;
        }
        if (all_dead)
            break;
    }
    announce("Overall minimum  : %.*e\n", DECIMAL_DIG+1, minval);
    kill(0, SIGINT);
}
