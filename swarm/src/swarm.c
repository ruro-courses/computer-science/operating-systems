#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <float.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>

#include "swarm.h"
#include "terminator.h"
#include "forkshare.h"

int sem_getval_safe(sem_t *sem)
{
    int ret;
    if (sem_getvalue(sem, &ret))
        terminate(errno, "Semaphore is broken (getvalue).\n");
    return ret;
}

void sem_wait_safe(sem_t *sem)
{
    while (sem_wait(sem))
        if (errno != EINTR)
            terminate(errno, "Semaphore is broken (wait).\n");
}

int sem_trywait_safe(sem_t *sem)
{
    while (sem_trywait(sem))
    {
        if (errno == EINTR)
            continue; // Interrupted
        if (errno == EAGAIN)
            return 1; // Worker is not ready
        terminate(errno, "Semaphore is broken (trywait).\n");
    }
    return 0;
}

void sem_post_safe(sem_t *sem)
{
    if (sem_post(sem))
        terminate(errno, "Semaphore is broken (post).\n");
}

void sem_init_safe(sem_t *sem, unsigned val)
{
    if (sem_init(sem, 1, val))
        terminate(errno, "Unable to initialize semaphore.\n");
}

TaskStream *makeTaskStream(char *files[], size_t mul)
{
    // Get memory.
    TaskStream *tstream = malloc(sizeof(TaskStream));
    if (!tstream)
        terminate(errno, "Couldn't allocate memory for the TaskStream.\n");
    
    // Size of blocks the files are broken into.
    tstream->step = d_pagesize();
    tstream->step *= mul ? mul : 1;

    // Null-terminated file list.    
    tstream->files = files;

    // First file, First byte.
    tstream->cur_file = 0;
    tstream->cur_off = 0;
    tstream->cur_max = 0;

    return tstream;
}

int getTask(TaskStream *tstream, Task *task)
{
    size_t c_file = tstream->cur_file;
    size_t c_step = tstream->step;
    char **s_files = tstream->files;
    struct stat filestat;
    if (!tstream->cur_max)
    {
        // No tasks left.
        if (!s_files[c_file])
            return 0;

        // Get next file.
        strncpy(task->filename, s_files[c_file], sizeof(task->filename));

        // Get next files size.
        if (stat(s_files[c_file], &filestat))
            terminate(errno, "Couldn't stat file %s.\n", s_files[c_file]);
        tstream->cur_max = filestat.st_size;
    }

    // Update values for next step.
    size_t c_max = tstream->cur_max;
    size_t c_off = tstream->cur_off;
    task->offset = c_off;
    task->amount = (c_step < c_max - c_off) ? c_step : c_max - c_off;
    tstream->cur_off += task->amount;

    if (tstream->cur_off == c_max)
    {
        tstream->cur_file++;
        tstream->cur_max = 0;
        tstream->cur_off = 0;
    }
    return 1;
}

Swarm *makeSwarm(size_t K)
{
    // Get shared mapping.
    Swarm *swarm = forkshare(sizeof(Swarm) + K*sizeof(Worker));
    if (!swarm)
        terminate(errno, "Unable to obtain shared memory for the Swarm.\n");
    swarm->worker_num = K;

    // Initialize all workers.
    for (size_t i = 0; i < K; i++)
        makeWorker(&swarm->workers[i]);

    return swarm;
}

void makeWorker(Worker *worker)
{
    // Initialize semaphores.
    sem_init_safe(&worker->hastask, 0);
    sem_init_safe(&worker->hasresult, 0);

    worker->keepfile = false;
    worker->alive = false;
    worker->task.filename[0] = '\0';

    // Spawn worker.
    pid_t p;
    p = fork();
    if (p == -1)
        terminate(errno, "Couldn't fork.\n");
    if (!p)
        workLoop(worker);
}

void pushTask(Worker *worker, Task *task)
{
    if (worker->alive)
        terminate(0, "Trying to push a task to a living worker.\n");
    
    // If still working on the same file, keep it open.
    worker->keepfile = !strcmp(task->filename, worker->task.filename);
    
    // Load next task.
    Task *t = &worker->task;
    t->offset = task->offset;
    t->amount = task->amount;
    strncpy(t->filename, task->filename, sizeof(t->filename));

    // Give the worker a task.
    worker->alive = true;
    sem_post_safe(&worker->hastask);
}

int popTask(Worker *worker, double *res)
{
    // Worker is dead only if not initialized or without a task.
    if (!worker->alive)
        return -1;

    // If worker finished, take over.
    if (sem_trywait_safe(&worker->hasresult))
        return 1;

    // Get worker results.
    *res = worker->result;
    worker->alive = false;
    return 0;
}

#define M_PN MAP_PRIVATE | MAP_NORESERVE

void workLoop(Worker *self)
{
    int fd = -1;
    double *src = NULL;
    Task t;
    while (true)
    {
        // Wait for the task.
        sem_wait_safe(&self->hastask);
        t = self->task;

        // Open/Update file.
        if (!self->keepfile)
        {
            if (fd != -1)
                close(fd);
            fd = open(t.filename, O_RDONLY);
        }
        if (fd == -1)
            terminate(errno, "Unable to fopen file %s.\n", t.filename);
        
        // Mmap file contents.
        src = mmap(NULL, t.amount, PROT_READ, M_PN, fd, t.offset);
        if (src == MAP_FAILED)
            terminate(errno, "Unable to mmap the file %s.\n", t.filename);

        // Find minimum in block.
        double res = DBL_MAX;
        double tmp;
        size_t cur = 0;
        size_t num = t.amount/sizeof(double);
        while (cur < num)
        {
            tmp = src[cur];
            res = (res < tmp) ? res : tmp;
            cur++;
        }

        // Unmap.
        if (munmap(src, t.amount))
            terminate(errno, "Unable to munmap file.\n");

        // Return result to overseer.
        self->result = res;
        sem_post_safe(&self->hasresult);
    }
}
