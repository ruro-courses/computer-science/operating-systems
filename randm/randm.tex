\documentclass[a4paper]{article}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[a4paper, top=0.75cm, bottom=0.75cm, left=2cm, right=2cm]{geometry}
\usepackage{listings}
\usepackage{inconsolata}
\pagenumbering{gobble}
\lstset{frame=single, showstringspaces=false, columns=fixed, basicstyle={\ttfamily}, commentstyle={\it}, numbers=left, tabsize=4}
\begin{document}
\lstset{title=Contents of \texttt{README}}
\lstset{
  language=,
  morekeywords={NAME, SYNOPSIS, DESCRIPTION, NOTES, EXAMPLES, AUTHOR, COPYRIGHT},
  moredelim=[is][\it]{<|}{|>},
  moredelim=[is][\bf]{<:}{:>}
}
\begin{lstlisting}
NAME
        <:randm:> - a library for generating top quality random integer and floating
        point values for you and your family. (1)

SYNOPSIS
        Integer random number generators:
            bool <:randbit:>(void);
            uint8_t <:rand8:>(void);
            uint16_t <:rand16:>(void);
            uint32_t <:rand32:>(void);
            uint64_t <:rand64:>(void);

        Fast floating point random number generators:
            double <:randd_f:>(void);
            double <:randd_segment_f:>(double, double);

        High Precision floating point random number generators:
            double <:randd_p:>(void);
            double <:randd_segment_p:>(double, double);

DESCRIPTION
        This library provides a portable set of pseudo-random number generators
        which rely on the `int rand(void)` function implemented in `stdlib.h`.

        The functions are guaranteed to work correctly with any value of
        <|RAND_MAX|>, but are optimized for values of <|RAND_MAX|> of form 2^<|N|> - 1,
        for large <|N|>.

        The 2 varieties of floating point random number generators are:
            Fast           - `<:randd_f:>` and `<:randd_segment_f:>`
            High Precision - `<:randd_p:>` and `<:randd_segment_p:>`

        The `<:randd_*:>` functions return values in range [0.0; 1.0]
        The `<:randd_segment_*:>` functions return values in range [a; b],
        where a and b are floating point values passed as arguments
            `<:randd_segment_*:>(a, b)`

NOTES
        (1): quality of random values is subjective and as such random values of
        lower quality are not admissible in a court of law as evidence of fraud,
        the random numbers may or may not be pseudo-random.

EXAMPLES
        Examples are available in src/examples.c

AUTHOR
        Implemented by Stotskii Andrey (ruro).

COPYRIGHT
        Copyright (c) 2017 Free Software Foundation, Inc.  License GPLv3+:
        GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
        This is free software: you are free to change and redistribute it.
        There is NO WARRANTY, to the extent permitted by law.
\end{lstlisting}
\newpage
\lstset{title=Contents of \texttt{makefile}}
\lstset{language={[gnu]make}}
\begin{lstlisting}
CFLAGS= -Wall -O3 -lm -std=gnu99
CC=gcc

vpath %.c src
vpath %.h src

examples: randm.o randm.h
randm.o: randm.h

.PHONY:libs
libs: randm.o

.PHONY:clean
clean:
	rm -f *.o examples
\end{lstlisting}
\lstset{language={[ANSI]C}}
\lstset{literate={float.}{float.}6}
\lstset{morekeywords={size_t, ssize_t, uintmax_t, bool, uint8_t, uint16_t, uint32_t, uint64_t, inline, Test}}
\lstset{title=Contents of \texttt{src/randm.h}}
\begin{lstlisting}
#ifndef _RANDM_H // INCLUDE GUARD
#define _RANDM_H

#include <math.h>
#include <float.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

// Get range of values for uint*_t and rand.
#define RANGE(x) ((uintmax_t)(x##_MAX) + 1)

// Get range of values as a double, non-overflowing.
#define DRANGE(x) ((double)(x##_MAX) + 1)

// Checks, wether [0;a] fits in [0;b] whole number of times.
#define FITS(a, b)  (RANGE(a) && !(RANGE(b) % RANGE(a)))

// Get the width of type in bits.
#define WIDTH(x) ((ssize_t) (sizeof(x) * CHAR_BIT))

// Get the number of 64 bit iterations needed for full precision.
#define DBL_MIN_ITER (1 - DBL_MIN_EXP/WIDTH(uint64_t))
#define DBL_EXTRA_ITER(x) (1 + ilogb(x)/WIDTH(uint64_t))

// Maximum value of bool. (for RANGE macro)
#define BIT_MAX 1

// Integer random declarations.
bool randbit(void);
uint8_t rand8(void);
uint16_t rand16(void);
uint32_t rand32(void);
uint64_t rand64(void);

// Fast random double declarations.
double randd_f(void);
double randd_segment_f(double, double);

// High Precision random double declarations.
double randd_p(void);
double randd_segment_p(double, double);

#endif // INCLUDE GUARD
\end{lstlisting}
\newpage
\lstset{title=Contents of \texttt{src/randm.c}}
\begin{lstlisting}
#include <math.h>
#include <float.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

#include "randm.h"

// On most systems the value of RAND_MAX is a large power of 2, allowing us to
// generate random integer values by simply finding the remainder of rand().
// On such systems, only the branch marked `SANE` will be compiled to short
// and efficient code, because FITS(type, RAND) will be always true.

// However if RAND_MAX is small, or not a power of 2, the condition is false
// and the random values are built up from individual bits, bytes or whatever
// the biggest `SANE` unit is.

// For example, on x86 under glibc:
//      randbit, rand8 and rand16 are `SANE` (1 rand() call each)
//      rand32 makes 2 rand() calls because of inlining 2 rand16() calls
//      rand64 makes 4 rand() calls because of inlining 2 rand32() calls

inline bool randbit(void)
{
    uintmax_t res;
    res = FITS(BIT, RAND) ?
            // SANE
        (rand() % RANGE(BIT)):
            // FALLBACK
        (rand() + rand() % 2);
    return res;
}

inline uint8_t rand8(void)
{
    uintmax_t res;
    res = FITS(UINT8, RAND) ?
            // SANE
        (rand() % RANGE(UINT8)):
            // FALLBACK
        ((randbit() << 0) |
        ( randbit() << 1) |
        ( randbit() << 2) |
        ( randbit() << 3) |
        ( randbit() << 4) |
        ( randbit() << 5) |
        ( randbit() << 6) |
        ( randbit() << 7));
     return res;
}

inline uint16_t rand16(void)
{
    uintmax_t res;
    res = FITS(UINT16, RAND) ?
            // SANE
        (rand() % RANGE(UINT16)):
            // FALLBACK
        ((uint16_t)rand8() << 0 |
         (uint16_t)rand8() << WIDTH(uint16_t)/2);
    return res;
}

\end{lstlisting}
\newpage
\begin{lstlisting}[firstnumber=last]
inline uint32_t rand32(void)
{
    uintmax_t res;
    res = FITS(UINT32, RAND) ?
            // SANE
        (rand() % RANGE(UINT32)):
            // FALLBACK
        ((uint32_t)rand16() << 0 |
         (uint32_t)rand16() << WIDTH(uint32_t)/2);
    return res;
}

inline uint64_t rand64(void)
{
    uintmax_t res;
    res = FITS(UINT64, RAND) ?
            // SANE
        (rand() % RANGE(UINT64)):
            // FALLBACK
        ((uint64_t)rand32() << 0 |
         (uint64_t)rand32() << WIDTH(uint64_t)/2);
    return res;
}

// Small doubles can have gaps between them *much* smaller, than 1/(RAND_MAX+1)
// as such the `trivial` implementation of a pseudo-random double generator
// will skip the values, which are far from all points N/(RAND_MAX+1), as such
// 2 versions of random double generators are available :
//      randd_f (fast) and randd_p (high precision)

// Additionally, a randd_segment* version exists, which accepts the lower and
// upper bound, where the random values should be selected.
//      randd*() is equivalent to randd_segment*(0.0, 1.0)

inline double randd_f(void)
{
    double res;
    res = rand64();
    res /= DRANGE(UINT64);
    return res;
}

inline double randd_p(void)
{
    double res = 0;
    for (int i = 0; i < DBL_MIN_ITER; i++)
    {
        res += rand64();
        res /= DRANGE(UINT64);
    }
    return res;
}

\end{lstlisting}
\newpage
\begin{lstlisting}[firstnumber=last]
inline double randd_segment_f(double a, double b)
{
    if (b < a)
        return nan("");
    double res = 0;
    res = rand64();
    res /= DRANGE(UINT64);
    return res*(b-a) + a;
}

inline double randd_segment_p(double a, double b)
{
    if (b < a)
        return nan("");
    double res = 0;
    for (int i = 0; i < DBL_MIN_ITER + DBL_EXTRA_ITER(b - a); i++)
    {
        res += rand64() * (b - a);
        res /= DRANGE(UINT64);
    }
    return res + a;
}
\end{lstlisting}
\lstset{title=Contents of \texttt{src/examples.c}}
\begin{lstlisting}
#include <time.h>
#include <stdio.h>
#include <inttypes.h>

#include "randm.h"

// Test function and it's name.
typedef struct
{
    void (*test)(void);
    char *name;
} Test;

// Make tests, fmt is the printf format, name is the name of the tested function
#define MKTEST(fmt, name)                                           \
{                                                                   \
    ({                                                              \
        /* Declare an anonymous function that runs the test */      \
        void f(void)                                                \
        {                                                           \
            /* Print the result of execution of `name` */           \
            printf(fmt, name);                                      \
        };                                                          \
        /* Export the declared function */                          \
        f;                                                          \
    }),                                                             \
    /* Stringify the name of function */                            \
    #name                                                           \
}

// Global variables for left and right bounds of randd_segment*.
double a = 0.0;
double b = 1.0;

\end{lstlisting}
\newpage
\begin{lstlisting}[firstnumber=last]
int main(void)
{
    // Load all available tests.
    Test tests[] =
    {
        MKTEST("%d ",        randbit()),
        MKTEST("%"PRIu8 " ", rand8()),
        MKTEST("%"PRIu16" ", rand16()),
        MKTEST("%"PRIu32" ", rand32()),
        MKTEST("%"PRIu64" ", rand64()),
        MKTEST("%g ",        randd_f()),
        MKTEST("%g ",        randd_p()),
        MKTEST("%g ",        randd_segment_f(a, b)),
        MKTEST("%g ",        randd_segment_p(a, b))
    };
    unsigned seed, option, iter;
    size_t testnum = sizeof(tests)/sizeof(*tests);

    // User I/O. (seed)
    printf("Please, input seed. (Or 0 for current time)\n");
    if ((scanf("%u", &seed) == 1) && seed)
        srand(seed);
    else
        srand(seed = time(NULL));

    printf("Setting seed to %u.\n", seed);

    // User I/O. (test selection)
    printf("Please select function to test:\n");
    for (unsigned i = 0; i < testnum; i++)
        printf("%u) %s\n", i + 1, tests[i].name);

    if ((scanf("%u", &option) == 1) && option && option <= testnum)
    {
        Test t = tests[option - 1];
        printf("Testing %s...\n", t.name);
        if (option > 7)
        {
            // Extra user I/O. ([a; b] bounds)
            printf("Please, input the lower and "
                    "upper bounds for random values.\n");
            scanf("%lf %lf", &a, &b);
        }

        // User I/O. (number of iterations)
        printf("Please, input the number of iterations.\n");
        if (scanf("%d", &iter) == 1)
            for (unsigned i = 0; i < iter; i++)
                t.test(); // Run tests.
    }
}
\end{lstlisting}
\end{document}
