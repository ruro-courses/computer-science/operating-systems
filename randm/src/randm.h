#ifndef _RANDM_H // INCLUDE GUARD
#define _RANDM_H

#include <math.h>
#include <float.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

// Get range of values for uint*_t and rand.
#define RANGE(x) ((uintmax_t)(x##_MAX) + 1)

// Get range of values as a double, non-overflowing.
#define DRANGE(x) ((double)(x##_MAX) + 1)

// Checks, wether [0;a] fits in [0;b] whole number of times.
#define FITS(a, b)  (RANGE(a) && !(RANGE(b) % RANGE(a)))

// Get the width of type in bits.
#define WIDTH(x) ((ssize_t) (sizeof(x) * CHAR_BIT))

// Get the number of 64 bit iterations needed for full precision.
#define DBL_MIN_ITER (1 - DBL_MIN_EXP/WIDTH(uint64_t))
#define DBL_EXTRA_ITER(x) (1 + ilogb(x)/WIDTH(uint64_t))

// Maximum value of bool. (for RANGE macro)
#define BIT_MAX 1

// Integer random declarations.
bool randbit(void);
uint8_t rand8(void);
uint16_t rand16(void);
uint32_t rand32(void);
uint64_t rand64(void);

// Fast random double declarations.
double randd_f(void);
double randd_segment_f(double, double);

// High Precision random double declarations.
double randd_p(void);
double randd_segment_p(double, double);

#endif // INCLUDE GUARD
