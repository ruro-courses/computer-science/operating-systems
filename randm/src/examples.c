#include <time.h>
#include <stdio.h>
#include <inttypes.h>

#include "randm.h"

// Test function and it's name.
typedef struct
{
    void (*test)(void);
    char *name;
} Test;

// Make tests, fmt is the printf format, name is the name of the tested function
#define MKTEST(fmt, name)                                           \
{                                                                   \
    ({                                                              \
        /* Declare an anonymous function that runs the test */      \
        void f(void)                                                \
        {                                                           \
            /* Print the result of execution of `name` */           \
            printf(fmt, name);                                      \
        };                                                          \
        /* Export the declared function */                          \
        f;                                                          \
    }),                                                             \
    /* Stringify the name of function */                            \
    #name                                                           \
}

// Global variables for left and right bounds of randd_segment*.
double a = 0.0;
double b = 1.0;

int main(void)
{
    // Load all available tests.
    Test tests[] =
    {
        MKTEST("%d ",        randbit()),
        MKTEST("%"PRIu8 " ", rand8()),
        MKTEST("%"PRIu16" ", rand16()),
        MKTEST("%"PRIu32" ", rand32()),
        MKTEST("%"PRIu64" ", rand64()),
        MKTEST("%g ",        randd_f()),
        MKTEST("%g ",        randd_p()),
        MKTEST("%g ",        randd_segment_f(a, b)),
        MKTEST("%g ",        randd_segment_p(a, b))
    };
    unsigned seed, option, iter;
    size_t testnum = sizeof(tests)/sizeof(*tests);

    // User I/O. (seed)
    printf("Please, input seed. (Or 0 for current time)\n");
    if ((scanf("%u", &seed) == 1) && seed)
        srand(seed);
    else
        srand(seed = time(NULL));

    printf("Setting seed to %u.\n", seed);

    // User I/O. (test selection)
    printf("Please select function to test:\n");
    for (unsigned i = 0; i < testnum; i++)
        printf("%u) %s\n", i + 1, tests[i].name);

    if ((scanf("%u", &option) == 1) && option && option <= testnum)
    {
        Test t = tests[option - 1];
        printf("Testing %s...\n", t.name);
        if (option > 7)
        {
            // Extra user I/O. ([a; b] bounds)
            printf("Please, input the lower and "
                    "upper bounds for random values.\n");
            scanf("%lf %lf", &a, &b);
        }

        // User I/O. (number of iterations)
        printf("Please, input the number of iterations.\n");
        if (scanf("%d", &iter) == 1)
            for (unsigned i = 0; i < iter; i++)
                t.test(); // Run tests.
    }
}
