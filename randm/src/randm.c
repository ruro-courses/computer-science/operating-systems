#include <math.h>
#include <float.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

#include "randm.h"

// On most systems the value of RAND_MAX is a large power of 2, allowing us to
// generate random integer values by simply finding the remainder of rand().
// On such systems, only the branch marked `SANE` will be compiled to short
// and efficient code, because FITS(type, RAND) will be always true.

// However if RAND_MAX is small, or not a power of 2, the condition is false
// and the random values are built up from individual bits, bytes or whatever
// the biggest `SANE` unit is.

// For example, on x86 under glibc:
//      randbit, rand8 and rand16 are `SANE` (1 rand() call each)
//      rand32 makes 2 rand() calls because of inlining 2 rand16() calls
//      rand64 makes 4 rand() calls because of inlining 2 rand32() calls

inline bool randbit(void)
{
    uintmax_t res;
    res = FITS(BIT, RAND) ?
            // SANE
        (rand() % RANGE(BIT)):
            // FALLBACK
        (rand() + rand() % 2);
    return res;
}

inline uint8_t rand8(void)
{
    uintmax_t res;
    res = FITS(UINT8, RAND) ?
            // SANE
        (rand() % RANGE(UINT8)):
            // FALLBACK
        ((randbit() << 0) |
        ( randbit() << 1) |
        ( randbit() << 2) |
        ( randbit() << 3) |
        ( randbit() << 4) |
        ( randbit() << 5) |
        ( randbit() << 6) |
        ( randbit() << 7));
     return res;
}

inline uint16_t rand16(void)
{
    uintmax_t res;
    res = FITS(UINT16, RAND) ?
            // SANE
        (rand() % RANGE(UINT16)):
            // FALLBACK
        ((uint16_t)rand8() << 0 |
         (uint16_t)rand8() << WIDTH(uint16_t)/2);
    return res;
}

inline uint32_t rand32(void)
{
    uintmax_t res;
    res = FITS(UINT32, RAND) ?
            // SANE
        (rand() % RANGE(UINT32)):
            // FALLBACK
        ((uint32_t)rand16() << 0 |
         (uint32_t)rand16() << WIDTH(uint32_t)/2);
    return res;
}

inline uint64_t rand64(void)
{
    uintmax_t res;
    res = FITS(UINT64, RAND) ?
            // SANE
        (rand() % RANGE(UINT64)):
            // FALLBACK
        ((uint64_t)rand32() << 0 |
         (uint64_t)rand32() << WIDTH(uint64_t)/2);
    return res;
}

// Small doubles can have gaps between them *much* smaller, than 1/(RAND_MAX+1)
// as such the `trivial` implementation of a pseudo-random double generator
// will skip the values, which are far from all points N/(RAND_MAX+1), as such
// 2 versions of random double generators are available :
//      randd_f (fast) and randd_p (high precision)

// Additionally, a randd_segment* version exists, which accepts the lower and
// upper bound, where the random values should be selected.
//      randd*() is equivalent to randd_segment*(0.0, 1.0)

inline double randd_f(void)
{
    double res;
    res = rand64();
    res /= DRANGE(UINT64);
    return res;
}

inline double randd_p(void)
{
    double res = 0;
    for (int i = 0; i < DBL_MIN_ITER; i++)
    {
        res += rand64();
        res /= DRANGE(UINT64);
    }
    return res;
}

inline double randd_segment_f(double a, double b)
{
    if (b < a)
        return nan("");
    double res = 0;
    res = rand64();
    res /= DRANGE(UINT64);
    return res*(b-a) + a;
}

inline double randd_segment_p(double a, double b)
{
    if (b < a)
        return nan("");
    double res = 0;
    for (int i = 0; i < DBL_MIN_ITER + DBL_EXTRA_ITER(b - a); i++)
    {
        res += rand64() * (b - a);
        res /= DRANGE(UINT64);
    }
    return res + a;
}
