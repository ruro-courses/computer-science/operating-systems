#ifndef _TICKER_H
#define _TICKER_H
#include <time.h>

typedef enum
{
    ACCUMULATE,
    INSPECT,
    INIT
} Action;

typedef void agent(double, Action, void **);

typedef struct
{
    agent *fun;
    void *usr;
} Actor;

typedef struct
{
    time_t timer;
    time_t sec_per_tick;
    Actor (*actors)[];
} TickState;

void tick(double, TickState *);

#endif
