#include <time.h>

#include "ticker.h"

void tick(double val, TickState *st)
{
    time_t now = time(NULL);
    Action upd = ACCUMULATE;

    if (!st->timer)
    {
        st->timer = now;
        upd = INIT;
    }
    
    if (now/st->sec_per_tick > st->timer/st->sec_per_tick)
    {
        st->timer = now;
        upd = INSPECT;
    }

    Actor *act = *(st->actors);
    for (int i = 0; act[i].fun; i++)
        act[i].fun(val, upd, &act[i].usr);

}
