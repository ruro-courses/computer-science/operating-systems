#include <time.h>
#include <math.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>

#include "stdagent.h"
#include "terminator.h"

typedef struct
{
    double avg;
    unsigned count;
} AverageAccumulator;

void accum_avg(AverageAccumulator *acc, double val)
{
    acc->avg = (acc->avg*acc->count + val)/(acc->count + 1);
    acc->count++;
}

void a_max(double val, Action act, void **data)
{
    double **max = (void *)data;
    
    if (act == INIT)
    {
        *max = malloc(sizeof(**max));
        if (!*max)
            terminate(errno, "Malloc failed.\n");
        **max = val;
    }
    
    if (act == INSPECT)
    {
        printf("%38s : %-38lf\n", "Maximum value", **max);
        **max = val;
    }
    
    **max = (**max > val) ? **max : val;
}

void a_min(double val, Action act, void **data)
{
    double **min = (void *)data;
    
    if (act == INIT)
    {
        *min = malloc(sizeof(**min));
        if (!*min)
            terminate(errno, "Malloc failed.\n");
        **min = val;
    }
    
    if (act == INSPECT)
    {
        printf("%38s : %-38lf\n", "Minimum value", **min);
        **min = val;
    }
    
    **min = (**min < val) ? **min : val;
}

void a_sum(double val, Action act, void **data)
{
    double **sum = (void *)data;
    
    if (act == INIT)
    {
        *sum = malloc(sizeof(**sum));
        if (!*sum)
            terminate(errno, "Malloc failed.\n");
        **sum = 0;
    }
    
    if (act == INSPECT)
    {
        printf("%38s : %-38lf\n", "Sum of values", **sum);
        **sum = 0;
    }
    
    **sum += val;
}

void a_time(double val, Action act, void **data)
{
    time_t **last = (void *)data;

    if (act == INIT)
    {
        *last = malloc(sizeof(**last));
        if (!*last)
            terminate(errno, "Malloc failed.\n");
        **last = time(NULL);
    }
    
    if (act == INSPECT)
    {
        char buf[39];
        strftime(buf, sizeof(buf), "%F %T", localtime(*last));
        printf("%38s : %-38s\n", "Stat time", buf);
        **last = time(NULL);
    }
}

void a_format(double val, Action act, void **data)
{
    if (!*data)
        terminate(0, "a_format failed, Actor.usr should be a string.\n");
    
    if (act == INSPECT)
        printf("%80s\n", (char *)*data);
}

void a_average(double val, Action act, void **data)
{
    AverageAccumulator **accum = (void *)data;
    
    if (act == INIT)
    {
        *accum = malloc(sizeof(**accum));
        if (!*accum)
            terminate(errno, "Malloc failed.\n");
        (*accum)->avg = 0;
        (*accum)->count = 0;
    }

    if (act == INSPECT)
    {
        printf("%38s : %-38lf\n", "Average of values", (*accum)->avg);
        (*accum)->avg = 0;
        (*accum)->count = 0;
    }

    accum_avg(*accum, val);
}

void a_deviation(double val, Action act, void **data)
{
    AverageAccumulator **accum = (void *)data;

    if (act == INIT)
    {
        *accum = malloc(2*sizeof(**accum));
        if (!*accum)
            terminate(errno, "Malloc failed.\n");
        (*accum)[0].avg = 0;
        (*accum)[1].avg = 0;
        (*accum)[0].count = 0;
        (*accum)[1].count = 0;
    }

    if (act == INSPECT)
    {
        double sq_avg = (*accum)[0].avg * (*accum)[0].avg;
        double avg_sq = (*accum)[1].avg;
        printf("%38s : %-38lf\n",
                "Standard deviation of values", sqrt(avg_sq - sq_avg));
        (*accum)[0].avg = 0;
        (*accum)[1].avg = 0;
        (*accum)[0].count = 0;
        (*accum)[1].count = 0;
    }

    accum_avg(&(*accum)[0], val);
    accum_avg(&(*accum)[1], val * val);
}
