#include <stdio.h>

#include "ticker.h"
#include "stdagent.h"

// DN is a string literal of N dash '-' characters.
#define D05 "-----"
#define D20 D05 D05 D05 D05
#define D80 D20 D20 D20 D20

int main(void)
{

    // An array of Actors, each Actor is a pair of {agent, data}:
    // agent is a function, which performs the action. (you can create your own)
    // data is a void *, used to pass extra data and preserve state.
    Actor act[] =
    {   
        {a_format,   D80},          // Print a pretty line of dashes.
        {a_time,     NULL},         // Print the timestamp of earliest data.
        {a_min,      NULL},         // Print the minimum.
        {a_max,      NULL},         // Print the maximum.
        {a_sum,      NULL},         // Print the sum.
        {a_average,  NULL},         // Print the average.
        {a_deviation,NULL},         // Print the standard deviation.
        {a_format,   D80},          // Print a pretty line of dashes.
        {NULL,       NULL}          // Actors array should be null terminated.
    };

    // A TickState completely contains all the state needed for the operation
    // of tick(). That is, hypothetically, tick() is thread safe and you can
    // run multiple tickers concurrently, with each having its own TickState.
    TickState tst =
    {
        .timer = 0,                 // New timer.
        .sec_per_tick = 60,         // 60 seconds per tick.
        .actors = &act              // Add the actors we just created.
    };

    double temp;
    while (scanf("%lf", &temp) == 1)
    {
        tick(temp, &tst);
    }
}
